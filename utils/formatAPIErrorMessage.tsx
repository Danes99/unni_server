// Error List

const ERROR_LIST = [
  {
    included_message: "Unique constraint failed on the fields: (`name`)",
    message: "Name already used"
  },
  {
    included_message: "Unique constraint failed on the fields: (`email`)",
    message: "Email already used"
  },
  {
    included_message: "Invalid credentials",
    message: "Invalid credentials"
  },
  {
    included_message: "Authentication token is invalid",
    message: 'Please Log In before attempting any interaction'
  },
  {
    included_message: "MenuIngredient_ingredientId_fkey",
    message: 'Ingredient is used in some menus'
  },
]

const formatAPIErrorMessage = (errorMessage: string): string => {

  for (let i = 0, c = ERROR_LIST.length; i < c; i++) {

    if (errorMessage.includes(ERROR_LIST[i].included_message)) {

      return ERROR_LIST[i].message
    }
  }

  return "Error"
}

export default formatAPIErrorMessage