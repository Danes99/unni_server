interface IOccurrence extends Object {
  occurrence: number
}

export const findOccurrence = (_array: object[], key: string): IOccurrence[] => {

  let occurrenceArray: IOccurrence[] = [];

  _array.forEach((x: any) => {

    // Checking if there is any object in occurrenceArray
    // which contains the key value
    if (occurrenceArray.some((val: any) => { return val[key] == x[key] })) {

      // If yes! then increase the occurrence by 1
      occurrenceArray.forEach((k: any) => {
        if (k[key] === x[key]) {
          k["occurrence"]++
        }
      })

    } else {
      // If not! Then create a new object initialize 
      // it with the present iteration key's value and 
      // set the occurrence to 1
      let a: any = {}

      // a[key] = x[key]
      a = { ...x }

      a["occurrence"] = 1
      occurrenceArray.push(a);
    }
  })

  return occurrenceArray
}

//  https://www.geeksforgeeks.org/how-to-count-number-of-occurrences-of-repeated-names-in-an-array-of-objects-in-javascript/