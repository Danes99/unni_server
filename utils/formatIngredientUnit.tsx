const formatIngredientUnit = (str: string) => {

  if (!str) return ''
  if (str === 'GRAM') return 'g'
  if (str === 'LITER') return 'L'
  return ''
}

export default formatIngredientUnit