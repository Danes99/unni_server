# FROM node:16.11.1-alpine3.11

# WORKDIR /usr/src/app

# # Copy only package.json and package-lock.json
# # for Docker layer optimisation
# COPY package.json package-lock.json /usr/src/app/
# RUN npm install --production
# COPY . /usr/src/app/
# EXPOSE 3000

# Build stage

FROM node:16.11.1-alpine3.11

WORKDIR /app

# Copy only package.json and package-lock.json
# for Docker layer optimisation
COPY ./package.json /app
COPY ./package-lock.json /app

# Compile state

RUN npm ci

COPY . /app

RUN npm run build

RUN rm -rf /node_modules

# Install stage

RUN npm ci --production

EXPOSE 80

# Run stage

CMD ["npm", "start"]