// tailwind.config.js
module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './config/css/**/*.{js,ts,jsx,tsx}',
  ],
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './config/css/**/*.{js,ts,jsx,tsx}'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'blue-unni': '#343796',
        'blue-secondary': '#595CA8',
        'blue-light-unni': '#EBEBF4',
        'gray-unni': '#EFF0F9',
        'red-unni': '#B81530',
        'red-light-unni': '#F8E8EA'

      },
      fontFamily: {
        montserrat: ["Montserrat", "sans-serif"],
      },
    },
    screens: {
      'phone': '356px',
      // => @media (min-width: 576px) { ... } 
      'tablet': '780px',
      // => @media (min-width: 960px) { ... } 
      'computer': '1024px',
      // => @media (min-width: 1440px) { ... }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    'tailwindcss',
    'postcss',
    'autoprefixer',
    require('@tailwindcss/forms')
  ],
}