// Import pre-installed components
import Image from 'next/image'
import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import { loadGetInitialProps } from 'next/dist/shared/lib/utils'


// Component props type
type ItemProps = {
  name: string,
  pic: string,
  color: string
}

function classNames(...classes: [string]) {
  return classes.filter(Boolean).join(' ')
}

const ItemBox = function Item({ name, pic, color }: ItemProps) {
  let [categories] = useState({
    CAFE: [
      {
        id: 1,
        title: 'Expresso',
        quantity: 0,
        img: '/DEA-White.png',
        color: '#2E7AF1',
        isSelected: false,
      },
      {
        id: 2,
        title: 'Longo',
        quantity: 0,
        img: '/user_w.png',
        color: '#2E7AF1',
        isSelected: false,
      },
      {
        id: 3,
        title: 'Capuccino',
        quantity: 0,
        img: '/user.png',
        color: '#2E7AF1',
        isSelected: false,
      },
    ],
    BIERE: [
      {
        id: 1,
        title: 'Blonde',
        quantity: 0,
        img: '/DEA-White.png',
        color: '#2E7AF1',
        isSelected: false,
      },
      {
        id: 2,
        title: 'Brune',
        quantity: 0,
        img: '/user_w.png',
        color: '#2E7AF1',
        isSelected: false,
      },
      {
        id: 3,
        title: 'Blanche',
        quantity: 0,
        img: '/user.png',
        color: '#2E7AF1',
        isSelected: false,
      },
    ]
  })
  let [isOpen, setIsOpen] = useState(false)

  function closeModal() {
    setIsOpen(false)
  }

  function openModal() {
    setIsOpen(true)
  }
  return (
    <>

      <div className=" filter drop-shadow-lg border-2 shadow-inner	rounded-2xl bg-gray-100 w-32 h-32 text-center flex flex-col items-center justify-around m-5 p-1 pt-3" style={{ borderColor: color }} onClick={openModal}>
        <div className="w-10 h-10 rounded-full mx-auto bg-blue-900 p-2xl p-1" style={{ background: color }}>
          <Image src={pic} alt="picture_product" width='50' height='50' />
        </div>
        <h1>{name}</h1>


        {/* {<Transition appear show={isOpen} as={Fragment}>
    <Dialog
      as="div"
      className="fixed z-10 inset-0 flex items-center justify-center"
      onClose={closeModal}
    >
      <div className=" px-4 text-center rounded-2xl bg-gray-500 opacity-90 h-3/5 w-1/2 flex items-center justify-center"> {/* overflow-auto //}
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Dialog.Overlay className="fixed inset-0" />
        </Transition.Child>

        {/* This element is to trick the browser into centering the modal contents. //}
        <span
          className="inline-block h-screen align-middle"
          aria-hidden="true"
        >
          &#8203;
        </span>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95"
        >
          <div className="inline-block w-full h-full p-6  overflow-auto text-left align-middle transition-all transform shadow-xl rounded-2xl">
          {Object.keys(categories).map((category) => (
          
            <Dialog.Title
              as="h3"
              key={category}
              className={classNames(
                'text-lg font-medium leading-6 text-white'
              )}
            >
                {category}
                {// <div className=" bg-white rounded-2xl row-start-1 col-start-1 col-span-4 flex flex-wrap justify-start md:justify-between overflow-auto">
                  {posts.map((post) => (
                    <Item name={post.title} pic={post.img} color={post.color}/>))}

                </div> //}
 
            </Dialog.Title>
          ))}
         
            <div className="mt-2">
              <p className="text-sm text-white">
                {/* afficher les sous-items  //}
              </p>
            </div>

            <div className="mt-4">
              <button
                type="button"
                className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                onClick={closeModal}
              >
                <span>Fermer <Image src="/remove.png" height="12" width="12"></Image></span>
              </button>
            </div>
          </div>
        </Transition.Child>
      </div>
    </Dialog>
  </Transition>} */}
      </div>
    </>
  )
}

export default ItemBox