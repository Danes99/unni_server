// Import pre-installed modules
// import { MouseEvent } from 'react'

// Import downloaded modules
import Cookies from 'js-cookie'
import { GraphQLClient, gql } from 'graphql-request'

// Import config
import { HOST_GRAPHQL_ENDPOINT } from '../../config/api/hostApi'

const queryCreateUserEvent = gql`
mutation Mutation($generatedAt: String!, $href: String!, $type: String!, $htmlId: String!) {
  createUserEvent(generatedAt: $generatedAt, href: $href, type: $type, htmlId: $htmlId) {
    id
  }
}
`

// Interfaces
type IProps = {
  children?: any
}

const sendErrorToAPI = async (data: any) => {
}

const sendDataToAPI = async (data: any) => {

  if (!data.type || !data.htmlId) return

  // Event
  const variables = {
    generatedAt: new Date(),
    href: window.location.href,
    ...data
  }

  try {

    const endpoint = window.location.origin + HOST_GRAPHQL_ENDPOINT
    const jwt = Cookies.get('jwt') as string

    const client = new GraphQLClient(
      endpoint,
      { headers: { authorization: jwt } }
    )

    await client.request(queryCreateUserEvent, variables)

  } catch (error) {
    console.error(error)
  }

}

const onClickHandler = async (event: any) => {

  const data = {
    type: event.type,
    htmlId: event.target.id
  }

  sendDataToAPI(data)
}

const DataCollectionWrapper = ({ children }: IProps) => {

  return (
    <div
      onError={(event) => { console.log('error') }}
      onClick={onClickHandler}
    >
      {children}
    </div>
  )
}

export default DataCollectionWrapper