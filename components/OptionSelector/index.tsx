// Import pre-installed modules
import { useState, Fragment } from 'react'

type LayoutProps = {
  children: any,
  page: string,
}

interface ITab {
  name: string
  current: boolean
}

interface IProps {
  options: ITab[]
}

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

const OptionSelector = ({ options }: IProps) => {

  // State
  const [, setDoRefresh] = useState(true)

  const updateTabHandler = (index: number) => {

    options.forEach((element: ITab) => {
      element.current = false
    })

    options[index].current = true

    setDoRefresh(current => !current)
  }

  return (
    <section className="">
      {/*role="group"*/}
      <div className="flex justify-evenly">
        {options.map((option: ITab, index: number) => (
          <button
            key={index}
            type="button"
            className="font-montserrat rounded-l rounded-r inline-block mx-1 px-1 py-1.5 bg-gray-200 text-blue-unni border-2 border-blue-unni font-bold text-xs leading-tight hover:bg-blue-unni hover:text-white focus:bg-blue-unni focus:text-white focus:outline-none focus:ring-0 active:bg-blue-unni transition duration-150 ease-in-out"
          >
            {option.name}
          </button>
        ))}
      </div>
    </section>
  );
}

export default OptionSelector
