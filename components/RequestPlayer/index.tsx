// Import framework modules
// import 

// Import downloaded modules
import { Player } from '@lottiefiles/react-lottie-player'

const PlayerLoading = <Player
  // className="w-96" 
  src="/lottiefiles/lottiefiles_loader_several_points_lf20.json"
  background="transparent"
  speed={1} loop autoplay
/>

const PlayerError = (callback: () => void) => <Player
  src="/lottiefiles/lottiefiles_error_lf30_editor.json"
  background="transparent"
  speed={1} autoplay loop
  onEvent={event => { if (event === 'complete') callback() }}
/>

const PlayerSuccess = (callback: () => void) => <Player
  // className="w-96" 
  src="/lottiefiles/lottiefiles_validated_lf30_editor.json"
  background="transparent" speed={1} autoplay
  onEvent={event => { if (event === 'complete') callback() }}
/>

interface IProps {
  status: string
  callbackSuccess: () => void
  callbackError: () => void,
  children?: JSX.Element
}

const RequestPlayer = ({ status, callbackSuccess, callbackError, children }: IProps) => {

  let display;

  switch (status) {
    case 'LOADING':
      display = PlayerLoading
      break;
    case 'DONE':
      display = PlayerSuccess(callbackSuccess)
      break;

    case 'DONE':
      display = PlayerError(callbackError)
      break;

    default:
      display = children || <div></div>
      break;
  }

  return display
}

export default RequestPlayer