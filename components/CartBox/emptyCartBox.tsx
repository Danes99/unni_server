// Import pre-installed components
// import Image from 'next/image'

// Component props type
// import * as LottiePlayer from "@lottiefiles/lottie-player";
import { Player } from '@lottiefiles/react-lottie-player';
import { useState } from 'react';

type CartProps = {
    name: string,
    pic: string,
    number: string
}

let [cartIsEmpty, setCartIsEmpty] = useState(true);

const Cart = function Item({ name, pic, number }: CartProps) {
    return (
        <>
            <div className="rounded-3xl bg-white row-start-1 col-start-5 col-span-2 p-2 text-center shadow-lg" >
                <h1 className="text-3xl pt-3">Votre panier est vide</h1>
                {/* <table className="table">
                    <thead>
                        <tr><th>Article</th><th>Prix</th><th>Quantité</th></tr>
                    </thead>
                    <tbody id="cart-tablebody"></tbody>
                </table>

                <p>Sous total : <span className="subtotal"></span>€</p> */}

                <button id="confirm-command" className="mt-3">Passe ta commande</button>
                {/* <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script> */}
                <Player src="https://assets10.lottiefiles.com/private_files/lf30_x2lzmtdl.json" background="transparent" speed={1} loop autoplay></Player>
                <p>{name}</p>
            </div>
        </>
    )
}
export default Cart



