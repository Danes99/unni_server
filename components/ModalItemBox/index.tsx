// Import pre-installed components
import Image from 'next/image'
import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import { loadGetInitialProps } from 'next/dist/shared/lib/utils'


// Component props type
export type typeItem = {
  id: number,
  title: string
}
export type typeItemModalProps = {
  items: [typeItem] | any
  isOpenned: boolean,
  closeModal: Function
}

function classNames(...classes: [string]) {
  return classes.filter(Boolean).join(' ')
}

export function ItemBoxModal({ items, isOpenned, closeModal }: typeItemModalProps) {

  return (
    <div className="flex flex-col justify-between h-full">

      {/* <div className=" filter drop-shadow-lg border-2 shadow-inner	rounded-2xl bg-gray-100 w-32 h-32 text-center flex flex-col items-center justify-around m-5 p-1 pt-3" style={{ borderColor: color}} onClick={openModal}>
      <div className="w-10 h-10 rounded-full mx-auto bg-blue-900 p-2xl p-1" style={{ background: color }}>
        <Image src={pic} alt="picture_product" width='50' height='50'/>
      </div>
      <h1>{name}</h1> */}

      <div className="flex flex-wrap justify-around mt-5 mb-5">
        {items.map((element: typeItem) => <div key={element.id}>
          <h1 className="filter drop-shadow-lg border-2 shadow-inner rounded-md bg-gray-100 w-24 h-12 flex items-center justify-around text-center mb-4 p-2">{element.title}</h1>
        </div>)}
      </div>

      <div className="mb-5 text-right mr-4">
        <button
          type="button"
          className="justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
          onClick={() => closeModal()}
        >
          <span>Fermer <Image src="/remove.png" height="12" width="12"></Image></span>
        </button>
      </div>

    </div>
  )
}

