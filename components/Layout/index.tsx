// Import pre-installed modules
import Head from "next/head"
import { Fragment } from 'react'

// Import downloaded modules
// import { Disclosure, Menu, Transition } from '@headlessui/react'
// import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'
// import classNames from 'classnames';

// Import custom components
import NavBar from '../NavBar'
import SideBar from '../SideBar'

type LayoutProps = {
  children: any,
  page: string,
}

const Layout = ({ children, page }: LayoutProps) => {
  return (
    <>
      <Head>
        {/*title dynamic */}
        <title>{page}</title>
        {/* <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script> */}
      </Head>

      {/* <div className="min-h-full"> */}
      <div className="h-screen w-screen">
        <div className="flex flex-col-reverse tablet:flex-row computer:flex-row h-full w-full">
          <SideBar />
          <div className=" flex flex-col w-full h-full"> {/* py-6 sm:px-6 lg:px-8      max-w-7xl mt-20 mx-auto flex-grow*/}
            <NavBar />
            <main className="p-4 h-full w-full overflow-auto" style={{ backgroundColor: "#F7F9FC" }}>
              {children}
            </main>
          </div>
        </div>
      </div>

    </>

  );
}

export default Layout