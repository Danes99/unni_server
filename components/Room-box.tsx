// Import pre-installed components
// import Image from 'next/image'

// Component props type
export { }
type RoomProps = {
  id: number;
  name: string;
  rooms: number;
  tenant: string;
  expense: number;
  dateIn: string;
  dateOut: string;
}

const Room = function Item({ id, name, rooms, tenant, expense, dateIn, dateOut }: RoomProps) {
  return (
    <>
      <div className=" filter drop-shadow-lg border-2 shadow-inner rounded-2xl bg-gray-100 w-12 h-12 text-center flex flex-col items-center justify-around m-5 p-1 pt-3">

        <h1>{id}</h1>

      </div>
    </>
  )
}
export default Room



