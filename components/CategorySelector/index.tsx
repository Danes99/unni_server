// Import pre-installed modules
import { useState, Fragment } from 'react'

// Import downloaded modules
// import { Disclosure, Menu, Transition, Switch } from '@headlessui/react'
// import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'
// import {
//   ArrowSmDownIcon,
//   ArrowSmUpIcon,
//   CreditCardIcon,
//   OfficeBuildingIcon,
//   UserIcon,
//   UsersIcon
// } from '@heroicons/react/solid'

// Import CSS
// import { TAILWINDCSS_CLASS_TITLE_DEFAULT } from '../../config/css/titles'

// Typescript interfaces
interface ITab {
  name: string
  href: string
  current: boolean
}

interface IProps {
  tabs: ITab[]
}

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

const CategorySelector = ({ tabs }: IProps) => {

  // State
  const [, setDoRefresh] = useState(true)

  const updateTabHandler = (index: number) => {

    tabs.forEach((element: ITab) => {
      element.current = false
    })

    tabs[index].current = true

    setDoRefresh(current => !current)
  }

  return (
    <section className="border-b border-gray-200 rounded-full">
      <nav className="font-montserrat flex space-x-8 justify-evenly" aria-label="Tabs">
        {tabs.map((tab: ITab, index: number) => (
          <a
            key={tab.name}
            href={tab.href}
            className={classNames(
              tab.current
                ? 'border-blue-unni border-b-4 text-blue-unni'
                : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300',
              'whitespace-nowrap py-4 px-1 border-b-4 font-medium text-sm flex-auto text-center'
            )}
            aria-current={tab.current ? 'page' : undefined}
            onClick={(event) => updateTabHandler(index)}
          >
            {tab.name}
          </a>
        ))}
      </nav>
    </section>
  )
}

export default CategorySelector
