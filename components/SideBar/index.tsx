// Import pre-installed components
// import Image from 'next/image'

// Component props type

import Head from "next/head"
import Link from 'next/link'
import Image from 'next/image'
import { Disclosure, Menu, Transition } from '@headlessui/react'
import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'
import classNames from 'classnames';
import { useState, Fragment } from 'react'

// import icons from react-icon
import { AiOutlineHome, AiTwotoneHome, AiOutlineShoppingCart } from 'react-icons/ai';
import { GiHamburgerMenu, GiHouseKeys, GiForkKnifeSpoon} from 'react-icons/gi';
import { RiRestaurant2Line } from 'react-icons/ri';
import { BsBoxSeam } from 'react-icons/bs';
import { MdOutlineDashboard, MdOutlineLogout} from 'react-icons/md';
import { FiEdit} from 'react-icons/fi';

// Import Routes Config
import { 
  ROUTE_TEST_RESTAURANT, 
  ROUTE_TEST_HOTEL,
  ROUTE_TEST_STOCK,
  ROUTE_TEST_MENU,
  ROUTE_TEST_DASHBOARD
} from "../../config/routes"

// ###### create component - icon #######
// list of items 
const ADD_ICONS: componentIcon[] = [
  { src: '/menu_blue.png', title: 'menu burger', h: 30, w: 30, current: false },
  { src: '/home.png', title: 'home', h: 30, w: 30, current: true },
  { src: '/restaurant_blue.png', title: 'restaurant', h: 30, w: 30, current: false },
  { src: '/key-chain_blue.png', title: 'hotel', h: 30, w: 30, current: false },
  { src: '/dashboard_blue.png', title: 'dashboard', h: 30, w: 30, current: false },
]

// Component icon
interface componentIcon {
  src: string;
  title: string;
  h: number;
  w: number;
  current: boolean
}

// Component
const createIcon = (newItem: componentIcon, index: number) => (
  <div
    key={index}
    className={classNames(
      newItem.current
        ? 'rounded-lg p-2 gap-4 bg-blue-unni'
        : 'rounded-lg',
      ' flex justify-center content-center'
    )}>
    <Image src={newItem.src} width={newItem.w} height={newItem.h}></Image>
  </div>
)

type LayoutProps = {
  children: any,
  page: string,
}

// creation of icon display
const SidebarIcon = ({icon}: any) =>(
  <div className="sidebar-icon">
    {icon}
  </div>
)

/* function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}  */
const SideBar = () => {

  return (
    <header className="flex shadow-lg top-0 left-0 m-0
                      tablet:h-screen tablet:w-20 tablet:flex-col tablet:justify-between
                      computer:h-screen computer:w-20 computer:flex-col computer:justify-between">
      <div className="flex tablet:flex-col computer:flex-col w-full items-center justify-evenly">
        <div className="hidden tablet:block computer:block">
          <SidebarIcon icon={<GiHamburgerMenu size="30"/> }/>
        </div>
        <Link href='/'>
          <div>
            <SidebarIcon icon={<AiOutlineHome size="30"/> }/>
          </div>
        </Link>
        <Link href={ROUTE_TEST_RESTAURANT}>
          <div>
            <SidebarIcon icon={<GiForkKnifeSpoon size="30"/> }/>
          </div>
        </Link>
        <Link href={ROUTE_TEST_HOTEL}>
          <div>
            <SidebarIcon icon={<GiHouseKeys size="30"/> }/>
          </div>
        </Link>
        <Link href={ROUTE_TEST_STOCK}>
          <div>
            <SidebarIcon icon={<BsBoxSeam size="30"/> }/>
          </div>
        </Link>
        <Link href={ROUTE_TEST_MENU}>
          <div>
            <SidebarIcon icon={<FiEdit size="30"/> }/>
          </div>
        </Link>
      </div>
      <div className="hidden tablet:block computer:block">
        <SidebarIcon icon={<MdOutlineLogout size="30"/> }/>
      </div>
    </header>
  )
}

export default SideBar
