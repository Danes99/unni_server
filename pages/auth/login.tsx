// Import pre-installed modules
import { GetStaticProps } from 'next'
import type { NextPage } from 'next'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useState, FormEvent } from 'react'

// Import downloaded modules
import { Message } from 'semantic-ui-react'
import { Player } from '@lottiefiles/react-lottie-player'
import { GraphQLClient, gql } from 'graphql-request'
import Cookies from 'js-cookie'

// Import custom components
import Layout from '../../components/Layout'

// Import custom functions
import formatAPIErrorMessage from '../../utils/formatAPIErrorMessage'

// Import config
import { HOST_GRAPHQL_ENDPOINT } from '../../config/api/hostApi'
import { ROUTE_USER, ROUTE_USER_REGISTER } from '../../config/routes'

// Constants: class names
const CLASS_NAME_FORM_INPUT = "form-control w-full mb-4 px-3 py-1.5 text-base font-normal text-gray-700 bg-gray-50 bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"

// Constants: initial state
const INITIAL_STATE_HAS_LOGIN_REQUEST_ENDED = false
const INITIAL_STATE_HAS_LOGIN_REQUEST_STARTED = false
const INITIAL_STATE_LOGIN_REQUEST_ERROR = ""

const userLogin = gql`
  mutation UserLogin($email: String!, $password: String!) {
    userLogin(email: $email, password: $password) {
      token,
      expiration
    }
  }
`

interface IFormTarget {
  email?: { value: string }
  password?: { value: string }
}

const UserLogin: NextPage = () => {

  // Next.js Router
  const router = useRouter()

  // State
  const [hasLoginRequestStarted, setHasLoginRequestStarted] = useState(INITIAL_STATE_HAS_LOGIN_REQUEST_STARTED)
  const [hasLoginRequestEnded, setHasLoginRequestEnded] = useState(INITIAL_STATE_HAS_LOGIN_REQUEST_ENDED)
  const [loginRequestError, setLoginRequestError] = useState(INITIAL_STATE_LOGIN_REQUEST_ERROR)

  const onSubmitHandler = async (event: FormEvent) => {
    event.preventDefault()

    setHasLoginRequestStarted(true)

    try {

      // const endpoint = `${API_PROTOCOL}//${API_BASE_URL}:${API_PORT}/${GRAPHQL_ENDPOINT}`
      const endpoint = HOST_GRAPHQL_ENDPOINT

      const target = event.target as IFormTarget

      const variables = {
        email: target.email?.value,
        password: target.password?.value
      }
      const client = new GraphQLClient(endpoint)
      const data = await client.request(userLogin, variables)

      Cookies.set(
        'jwt',
        data.userLogin.token,
        { path: '/', expires: new Date(data.userLogin.expiration) }
      )

    } catch (error) {
      console.error(error)
      setLoginRequestError(
        formatAPIErrorMessage(error instanceof Error ? error.message : 'error')
      )
    }

    setHasLoginRequestEnded(true)
  }

  const loginForm = <>
    <div className="flex flex-col items-center place-items-center place-content-center bg-white w-full h-full py-8">

      <Image
        src='/logo/unni.svg'
        alt='logo'
        height='140px'
        width='400px'
      />

      <form className="mt-10 w-2/3 flex flex-col tablet:w-1/2" onSubmit={onSubmitHandler}>
        <input autoFocus
          className={CLASS_NAME_FORM_INPUT}
          id="email" placeholder="Email" type="email" required />
        <input autoFocus
          className={CLASS_NAME_FORM_INPUT}
          id="password" placeholder="Password" type="password" required />
        <div className="flex flex-row gap-2">
          <a className="text-xs text-red-unni mt-4 cursor-pointer w-1/2 h-full text-center self-center">Forgot password?</a>
          <button className="text-sm text-center bg-blue-unni text-white py-1 rounded font-medium w-1/2">
            Log In
          </button>
        </div>
      </form>

      <div className="flex justify-evenly space-x-2 w-1/2 mt-4">
        <span className="bg-gray-300 h-px flex-grow t-2 relative top-2"></span>
        <span className="flex-none uppercase text-xs text-gray-400 font-semibold">or</span>
        <span className="bg-gray-300 h-px flex-grow t-2 relative top-2"></span>
      </div>
      <span className="text-sm mt-2">Don&apos;t have an account ?</span>
      <Link href={ROUTE_USER_REGISTER}>
        <a className="text-blue-unni text-sm font-semibold">Sign up</a>
      </Link>
    </div>

  </>

  return (
    <Layout page='Login'>
      <div className="flex flex-row h-full m-0">
        {/* Login section */}
        <section className="flex flex-col rounded-md justify-center tablet:rounded-l-md h-full w-full tablet:w-1/2">

          {hasLoginRequestStarted ?
            hasLoginRequestEnded ?
              loginRequestError ?
                <div className="w-96">
                  <Player src="/lottiefiles/lottiefiles_error_lf30_editor.json" background="transparent" speed={1} autoplay loop />
                  <div className="mt-20">
                    <Message negative>
                      <Message.Header>Error</Message.Header>
                      <p>{loginRequestError}</p>
                    </Message>
                  </div>
                </div>
                :
                <Player className="w-96" src="/lottiefiles/lottiefiles_validated_lf30_editor.json" background="transparent" speed={1} autoplay
                  onEvent={(event: string) => { if (event === 'complete') router.push(ROUTE_USER) }}
                />
              :
              <Player className="w-96" src="/lottiefiles/lottiefiles_loader_several_points_lf20.json" background="transparent" speed={1} loop autoplay />
            :
            loginForm
          }

        </section>
        {/* Illustration section, hidden on mobile devices */}
        <section className="hidden bg-white items-center place-items-center place-content-center tablet:flex tablet:justify-center tablet:rounded-r-md tablet:w-1/2">
          <Player
            src="/lottiefiles/95530-password.json"
            background="transparent"
            speed={1} autoplay loop

          // onEvent={event => { if (event === 'complete') router.push(ROUTE_HOME) }}
          />
        </section>
      </div>
    </Layout>
  )
}

export default UserLogin
