// Import pre-installed modules
import { GetStaticProps } from 'next'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import Image from 'next/image'
import Link from 'next/link'
import { useState, FormEvent } from 'react'

// Import downloaded modules
import { Message } from 'semantic-ui-react'
import { Player } from '@lottiefiles/react-lottie-player'
import { GraphQLClient, gql } from 'graphql-request'
import Cookies from 'js-cookie'

// Import custom components
import Layout from '../../components/Layout'

// Import custom functions
import formatAPIErrorMessage from '../../utils/formatAPIErrorMessage'

// Import config
import { HOST_GRAPHQL_ENDPOINT } from '../../config/api/hostApi'
import { ROUTE_USER, ROUTE_USER_LOGIN } from '../../config/routes'

// Constants: class names
const CLASS_NAME_FORM_INPUT = "form-control w-full mb-4 px-3 py-1.5 text-base font-normal text-gray-700 bg-gray-50 bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"

const createUser = gql`
  mutation CreateUser($name: String!, $email: String!, $password: String!) {
    createUser(name: $name, email: $email, password: $password) {
      expiration
      token
    }
  }
`

interface IFormTarget {
  name?: { value: string }
  email?: { value: string }
  password?: { value: string }
}

// Constants: initial state
const INITIAL_STATE_HAS_REGISTER_REQUEST_ENDED = false
const INITIAL_STATE_HAS_REGISTER_REQUEST_STARTED = false
const INITIAL_STATE_REGISTER_REQUEST_ERROR = ""

const INITIAL_STATE_INPUT_PASSWORD = ""
const INITIAL_STATE_INPUT_PASSWORD_2 = ""

const UserRegister: NextPage = () => {

  // Next.js Router
  const router = useRouter()

  // State
  const [hasRegisterRequestStarted, setHasRegisterRequestStarted] = useState(INITIAL_STATE_HAS_REGISTER_REQUEST_STARTED)
  const [hasRegisterRequestEnded, setHasRegisterRequestEnded] = useState(INITIAL_STATE_HAS_REGISTER_REQUEST_ENDED)
  const [registerRequestError, setRegisterRequestError] = useState(INITIAL_STATE_REGISTER_REQUEST_ERROR)
  const [inputPassword, setInputPassword] = useState(INITIAL_STATE_INPUT_PASSWORD)
  const [inputPassword2, setInputPassword2] = useState(INITIAL_STATE_INPUT_PASSWORD_2)

  const onSubmitHandler = async (event: FormEvent) => {
    event.preventDefault()

    setHasRegisterRequestStarted(true)

    try {

      // const endpoint = `${API_PROTOCOL}//${API_BASE_URL}:${API_PORT}/${GRAPHQL_ENDPOINT}`
      const endpoint = HOST_GRAPHQL_ENDPOINT
      const target = event.target as IFormTarget
      const variables = {
        name: target.name?.value,
        email: target.email?.value,
        password: target.password?.value
      }
      const client = new GraphQLClient(endpoint)
      const data = await client.request(createUser, variables)

      Cookies.set(
        'jwt',
        data.createUser.token,
        { path: '/', expires: new Date(data.createUser.expiration) }
      )

    } catch (error) {
      console.error(error)
      setRegisterRequestError(
        formatAPIErrorMessage(error instanceof Error ? error.message : 'error')
      )
    }

    setHasRegisterRequestEnded(true)
  }

  const doPasswordsMismatch = inputPassword !== inputPassword2
  const doDisplayMismatchErrorMessage = inputPassword2.length > 0 && doPasswordsMismatch

  const registerForm = <>
    <div className="flex flex-col items-center place-items-center place-content-center bg-white w-full h-full py-8 ">

      <Image
        src='/logo/unni.svg'
        alt='logo'
        height='140px'
        width='400px'
      />

      <form className="mt-10 w-1/2 flex flex-col" onSubmit={onSubmitHandler}>
        <input
          autoFocus required
          className={CLASS_NAME_FORM_INPUT}
          id="name" placeholder="Name" type="text" />
        <input
          autoFocus required
          className={CLASS_NAME_FORM_INPUT}
          id="email" placeholder="Email" type="text" />
        <input
          autoFocus required
          onChange={event => setInputPassword(event.currentTarget.value)}
          className={CLASS_NAME_FORM_INPUT}
          id="password" placeholder="Password" type="password" />
        <input
          autoFocus required
          onChange={event => setInputPassword2(event.currentTarget.value)}
          className={CLASS_NAME_FORM_INPUT}
          id="password2" placeholder="Verify Password" type="password" />

        <button className="text-sm text-center bg-blue-unni text-white py-1 rounded font-medium w-full h-10"
          disabled={doPasswordsMismatch}>
          Sign Up
        </button>
      </form>
      <div className="flex justify-evenly space-x-2 w-1/2 mt-4">
        <span className="bg-gray-300 h-px flex-grow t-2 relative top-2"></span>
        <span className="flex-none uppercase text-xs text-gray-400 font-semibold">or</span>
        <span className="bg-gray-300 h-px flex-grow t-2 relative top-2"></span>
      </div>
      <span className="text-sm mt-2">Already have an account ?</span>
      <Link href={ROUTE_USER_LOGIN}>
        <a className="text-blue-unni text-sm font-semibold">Sign in</a>
      </Link>
    </div>
  </>

  return (
    <Layout page='Register'>
      <div className='flex flex-row w-full h-full'>
        {/* Login section */}
        <section className="flex flex-col rounded-l justify-center h-full tablet:w-1/2">

          {hasRegisterRequestStarted ?
            hasRegisterRequestEnded ?
              registerRequestError ?
                <div className="w-96">
                  <Player src="/lottiefiles/lottiefiles_error_lf30_editor.json" background="transparent" speed={1} autoplay loop />
                  <div className="mt-20">
                    <Message negative>
                      <Message.Header>Error</Message.Header>
                      <p>{registerRequestError}</p>
                    </Message>
                  </div>
                </div>
                :
                <Player className="w-96" src="/lottiefiles/lottiefiles_validated_lf30_editor.json" background="transparent" speed={1} autoplay
                  onEvent={(event: string) => { if (event === 'complete') router.push(ROUTE_USER) }}
                />
              :
              <Player className="w-96" src="/lottiefiles/lottiefiles_loader_several_points_lf20.json" background="transparent" speed={1} loop autoplay />
            :
            registerForm
          }

        </section>
        {/* Illustration section, hidden on mobile devices */}
        <section className="hidden bg-white tablet:flex tablet:flex-col tablet:w-1/2">

        </section>
      </div>
    </Layout>
  )
}

export default UserRegister
