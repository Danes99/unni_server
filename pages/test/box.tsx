// Import pre-installed modules
import React, { useState, useEffect, useRef, Fragment } from 'react';


// Import downloaded modules
import { Tab } from '@headlessui/react';

// Import custom components
import Layout from "../../components/Layout";
import Cart from "../../components/CartBox/Cart-box";
import Item from "../../components/Item-box";
import data from "../../dataBase/data.json";
import { Dialog, Transition } from '@headlessui/react'
import { typeItemModalProps, typeItem, ItemBoxModal } from '../../components/ModalItemBox';

function classNames(...classes: [string]) {
  return classes.filter(Boolean).join(' ')
}

export default function Box() {

  function displayModal(id: number, items: [typeItem] | any) {
    setTransitionItemList(items)
    setDoDisplayModal(true)
  }

  let [doDisplayModal, setDoDisplayModal] = useState(false);
  let [transitionItemList, setTransitionItemList] = useState([]);


  return (
    <Layout page="Bar">
      <div className="min-w-full max-w-md  sm:px-0 bg-gray-100 rounded-2xl" >
        <Tab.Group>
          <Tab.Panels>
            {Object.values(data).map((posts, idx) => (
              <Tab.Panel
                key={idx}
                className={classNames(
                  'bg-gray-100 rounded-xl p-3 grid grid-flow-col grid-rows-1 grid-cols-6 gap-4 focus:outline-none'
                )}
              >
                <div className=" relative bg-white shadow-lg rounded-2xl row-start-1 col-start-1 col-span-4 flex flex-wrap justify-start md:justify-between overflow-auto">
                  {posts.map((post) => (

                    <div onClick={() => displayModal(post.id, post.items)} key={post.id}>
                      <Item name={post.title} pic={post.img} color={post.color} />

                    </div>
                  ))}
                  {doDisplayModal ?
                    <Transition
                      appear
                      show={doDisplayModal}
                      className="duration-200 transition-shadow bg-gray-600 bg-opacity-75 inset-0 bottom-0 relative md:absolute">
                      <ItemBoxModal items={transitionItemList} isOpenned={doDisplayModal} closeModal={() => setDoDisplayModal(false)} />
                    </Transition>
                    :
                    null}

                </div>

                <Cart name={''} pic={''} number={''} />

              </Tab.Panel>
            ))}
          </Tab.Panels>
          <Tab.List className="flex p-1 space-x-10  rounded-xl m-2 pb-4 ">
            {Object.keys(data).map((category) => (
              <Tab
                key={category}
                className={({ selected }) =>
                  classNames(

                    'shadow-lg bg-white w-full py-2.5 text-sm leading-5 font-medium text-black rounded-lg focus:outline-none focus:ring-2 ring-offset-2 ring-offset-blue-400 ring-white ring-opacity-60 ',
                    /*  selected
                       ? 'bg-white shadow'
                       : 'text-blue-100 hover:bg-white/[0.12] hover:text-gray-600' */
                  )
                }
              >
                {category}
              </Tab>
            ))}
          </Tab.List>
        </Tab.Group>
      </div>
    </Layout>
  );
}

