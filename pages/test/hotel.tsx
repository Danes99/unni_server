import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Layout from '../../components/Layout'
import CategorySelector from '../../components/CategorySelector'
import OptionSelector from '../../components/OptionSelector'
import { Item } from 'semantic-ui-react'

import { TAILWINDCSS_CLASS_TITLE_DEFAULT } from '../../config/css/titles'

import { DateRange, DateRangePicker } from 'react-date-range';
import { render } from '@headlessui/react/dist/utils/render';

import React, { useState } from "react";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import { Calendar } from "react-modern-calendar-datepicker";
import type { DayRange } from "react-modern-calendar-datepicker";
import DatePicker from 'react-modern-calendar-datepicker';


/*import ItemSelector from '../components/ItemSelector'*/

/*const INITIAL_STATE_TABS = [
  { name: 'Orders', href: '#waiting', current: true },
  { name: 'Cooking', href: '#cooking', current: false },
  { name: 'To pay', href: '#to_pay', current: false },
  { name: 'Payed', href: '#payed', current: false },
]*/

const INITIAL_STATE_TABS = [
  { name: 'Category #1', href: '#category1', current: true },
  { name: 'Category #2', href: '#category2', current: false },
  { name: 'Category #2', href: '#category2', current: false },
]

const INITIAL_STATE_OPTIONS = [
  { name: 'Option #1', current: true },
  { name: 'Option #2', current: false },
  { name: 'Option #3', current: false }
]
//########################## Items ##########################

// list of test items 
const TEST_ADD_ITEMS: componentItem[] = [
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 13 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 13 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 73 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 71 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 73 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 13 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 71 },
  { src: '/beer.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/beer.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 13 }
]

// Component items/product
interface componentItem {
  src: string;
  title: string;
  contenance: number;
  price: number;
  quantity: number
}

// Items's Component
const createComponent = (newItem: componentItem, index: number) => (
  <div key={index} className="border-4 border-blue-unni rounded-lg mt-4 p-2 flex flex-col justify-between w-28 h-28 space-y-2 bg-gray-unni
                              computer:w-44 computer:h-44 
                              tablet:w-44 tablet-44 ">
    <div className='flex flex-row justify-center space-x-3 '>
      <p>{newItem.title}</p>
      <p>{newItem.contenance}cl</p>
    </div>
    <div className="rounded-full p-4 h-20 w-20 grid bg-white border-blue-unni border-4 self-center">
      <Image src={newItem.src} width={30} height={30}></Image>
    </div>
    <div className='flex flex-row space-x-3 justify-center'>
      <p>{newItem.price}€</p>
      <p>°</p>
      <p>{newItem.quantity} produits</p>
    </div>

  </div>
)
//########################## Empty menu ##########################

// Time
const defaultTime = {
  hour: (new Date).getHours(),
  minutes: (new Date).getMinutes()
}

// empty menu Component
const emptyMenuComponent = (index: number) => {
  const defaultTime = {
    hours: (new Date).getHours(),
    minutes: (new Date).getMinutes()
  }
  console.log('emptyMenuComponent')
  return (
    <div key={index} className='flex flex-col justify-center'>
      <h2>Nouvelle ordere</h2>
      <h3>Selectionner un article</h3>
      <Image src="./cart.png" width={80} height={80}></Image>
      <div className='flex flex-col'>
        <h3>{defaultTime.hours}:</h3>
        <h3>{defaultTime.minutes}</h3>
      </div>
    </div>
  )
}


const defaultFrom = {
  year: (new Date).getFullYear(),
  month: (new Date).getUTCMonth() + 1,
  day: (new Date).getDate(),
};

const defaultValue: DayRange = {
  from: defaultFrom,
  to: defaultFrom,
};

// calendar's content
const CalendarItem = (index: number) => {
  const [selectedDayRange, setSelectedDayRange] = useState(
    defaultValue
  );
  return (
    <div key={index}>
      <Calendar
        value={selectedDayRange}
        onChange={setSelectedDayRange}
        colorPrimary="#343796"
        colorPrimaryLight="#EBEBF4"
        calendarClassName="responsive-calendar" // added this
      // calendarClassName="font-size:20px"
      />
    </div>
  )
}



const Hotel: NextPage = () => {
  return (
    <Layout page='Hotel'>
      <div className='w-full flex flex-col tablet:flex-row tablet:gap-x-3 tablet:h-full justify-between overflow-hidden'>
        {/* Left-side of layout - Categories and items */}
        <article className='flex flex-col tablet:block tablet:h-max tablet:w-2/3 computer:w-3/4 tablet:overflow-auto computer:overflow-auto'>
          <div className='tablet:block computer:block'>
            { /* <ItemSelector /> */}

            {/* searchBar + cart */}
          </div>
          <div className='grid gap-y-4 computer:grid-cols-5 tablet:grid-cols-4 justify-items-center'>
            {TEST_ADD_ITEMS.map(
              (element: componentItem, index: number) => createComponent(element, index)
            )}

          </div>
          <section>

          </section>
        </article>
        {/* Right-side of layout - Ticket and Payments info */}
        <aside className='hidden flex flex-col items-stretch rounded py-1 divide-gray-400 
                          tablet:block tablet:h-full tablet:w-1/3 
                          computer:block computer:w-1/4' style={{ backgroundColor: '#EFF0F9' }}>
          {(index: number) => emptyMenuComponent(index)}
          <h3>zahdaz</h3>
        </aside>
      </div>
    </Layout>
  )
}

export default Hotel
