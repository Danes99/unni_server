import type { NextPage } from "next";
import Layout from "../../components/Layout";
import Image from "next/image";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import { AiOutlineEuroCircle } from "react-icons/ai";

import InputAdornment from "@mui/material/InputAdornment";
import DesktopDatePicker from "@mui/lab/DesktopDatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";

import * as React from "react";
import { map, style } from "d3";
import classNames from "classnames";
import { useState, Fragment } from "react";

const ITEMS_LIST: componentSelector[] = [
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
];

interface componentSelector {
  title: string;
  src: string;
  price: number;
  quantity: number;
}
// right side empty cart component
const EmptyComponent = () => (
  <div className="h-full flex flex-col items-center gap-y-16">
    <h1>Nouvelle commande</h1>
    <h3>Selectionnez un article</h3>
    <div className="w-40 h-40 flex flex-col justify-center items-center">
      <Image src={"/cart.png"} width={180} height={180}></Image>
    </div>
  </div>
);

// item for phone/tablet component
const ItemComponent = (item: componentSelector, index: number) => (
  <div
    key={index}
    className="border-2 border-blue-unni rounded-lg p-2 flex flex-col justify-between items-center w-36 h-36 bg-gray-unni
                    computer:w-44 computer:h-44 computer:border-4 computer:space-y-2
                    tablet:w-44 tablet-44 tablet:border-4 tablet:space-y-2"
  >
    <div
      className="text-xs
                        tablet:text-base
                        computer:text-base"
    >
      {item.title}
    </div>
    <div
      className="rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
                        tablet:border-4 tablet:h-20 tablet:w-20
                        computer:border-4 computer:h-20 computer:w-20"
    >
      <Image src={item.src} width={25} height={25}></Image>
    </div>
    <div className="space-x-2 text-xs">
      <span>{item.price}€</span>
      <span>.</span>
      <span>{item.quantity} produit(s)</span>
    </div>
  </div>
);

// item for edit component
const ItemEditComponent = () => (
  <div
    className="border-4 border-blue-unni rounded-lg flex flex-col justify-between items-center space-y-2 bg-gray-unni
                    computer:w-44 computer:h-52 
                    tablet:w-44 tablet:h-52"
  >
    <span className="text-base font-montserrat">Canette CocaCola</span>
    <div
      className="rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
                        tablet:border-4 tablet:h-20 tablet:w-20
                        computer:border-4 computer:h-20 computer:w-20"
    >
      <Image src={"/cart.png"} width={30} height={30}></Image>
    </div>
    <div className="space-x-2 text-xs">
      <span>2,3€</span>
      <span>.</span>
      <span>45 produit(s)</span>
    </div>
    <button className="h-12 w-full bg-blue-secondary space-x-2 text-sm flex items-center justify-center">
      <span>
        <Image src={"/edit.png"} width={20} height={20}></Image>
      </span>
      <span className="text-white">Modifier</span>
    </button>
  </div>
);

// item for edit dotted component
const ItemEditDashedComponent = () => (
  <button
    className="border-4 border-dashed border-blue-unni rounded-lg flex flex-col justify-center items-center bg-gray-unni
                    computer:w-44 computer:h-52 
                    tablet:w-44 tablet:h-52"
  >
    <div className="h-20 w-20 flex flex-col items-center justify-center">
      <Image src={"/plus.png"} width={40} height={40}></Image>
    </div>
    <p>Ajouter</p>
  </button>
);

// Creation of menu, add selected ingredient component
const SelectIngredientComponent = () => (
  <div
    className="rounded-lg flex flex-col justify-between items-center bg-gray-unni
                    computer:w-96 computer:h-full 
                    tablet:w-96 tablet:h-full"
  >
    <h1>Ingrédients</h1>
    <div className="h-10 w-72 bg-white rounded-lg flex flex-row justify-around mb-2">
      <input placeholder="Rechercher"></input>
      <div className="h-10 w-10 flex flex-col items-center justify-center">
        <Image src={"/search.png"} width={20} height={20}></Image>
      </div>
    </div>
    <div className="flex flex-wrap  justify-center gap-y-1 gap-x-2 overflow-auto h-full mt-2">
      {ITEMS_LIST.map((element: componentSelector, index: number) =>
        ItemComponent(element, index)
      )}
    </div>
    <button className="text-white font-montserrat bg-blue-unni rounded-lg h-12 w-72 mb-2 mt-2">
      Ajouter
    </button>
  </div>
);

// item in line
const LineItem = () => (
  <div className="flex flex-row h-12 items-center justify-around">
    <div className="rounded-full border-2 border-blue-unni bg-white h-8 w-8 flex flex-row justify-center items-center">
      <Image src={"/beer.png"} width={20} height={20}></Image>
    </div>
    <span>CocaCola 33cl</span>
    <div className="rounded-lg h-8 w-8 border-blue-unni bg-white border-2 flex flex-row items-center justify-center">
      2
    </div>
    <div className="rounded-lg h-8 w-8 border-red-600 bg-white border-2 flex flex-row items-center justify-center">
      <Image src={"/remove.png"} width={20} height={20}></Image>
    </div>
  </div>
);
const CLASS_NAME_BUTTON_MENU_DEFAULT =
  "bg-white rounded-lg p-2 w-28 transition duration-500 font-montserrat hover:bg-blue-unni hover:text-white";
const CLASS_NAME_BUTTON_MENU_FOCUS =
  "bg-blue-unni rounded-lg p-2 w-28 text-white font-montserrat";

const categories = [
  {
    value: "Boisson",
  },
  {
    value: "Plat",
  },
  {
    value: "Dessert",
  },
  {
    value: "Snack",
  },
];

interface IPropsEditMenuComponent {
  displayIgredientList: any;
  hideIgredientList: any;
}

// edit menu / drinks and ingredients
const EditMenuComponent = ({
  displayIgredientList,
  hideIgredientList,
}: IPropsEditMenuComponent) => {
  const [isMenuOne, setIsMenuOne] = useState(CLASS_NAME_BUTTON_MENU_DEFAULT);
  const [isMenuTwo, setIsMenuTwo] = useState(CLASS_NAME_BUTTON_MENU_DEFAULT);
  const [isMenuThree, setIsMenuThree] = useState(CLASS_NAME_BUTTON_MENU_FOCUS);

  const resetStateVariables = () => {
    setIsMenuOne(CLASS_NAME_BUTTON_MENU_DEFAULT);
    setIsMenuTwo(CLASS_NAME_BUTTON_MENU_DEFAULT);
    setIsMenuThree(CLASS_NAME_BUTTON_MENU_DEFAULT);
  };

  //   const handleClick = (e: any) => {
  //     resetStateVariables();
  //     e.target.className = CLASS_NAME_BUTTON_MENU_FOCUS;
  //   };
  const handleClickMenuOne = (e: any) => {
    resetStateVariables();
    setIsMenuOne(CLASS_NAME_BUTTON_MENU_FOCUS);
    displayIgredientList();
    // console.log("menu1");
  };
  const handleClickMenuTwo = (e: any) => {
    resetStateVariables();
    setIsMenuTwo(CLASS_NAME_BUTTON_MENU_FOCUS);
    displayIgredientList();
    // console.log("menu2");
  };
  const handleClickMenuThree = (e: any) => {
    resetStateVariables();
    setIsMenuThree(CLASS_NAME_BUTTON_MENU_FOCUS);
    hideIgredientList();
  };

  const [category, setCategory] = React.useState("Plat");
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCategory(event.target.value);
  };
  const [value, setValue] = React.useState<Date | null>(new Date());

  const handleChangeDate = (newValue: Date | null) => {
    setValue(newValue);
  };
  return (
    <div
      className="rounded-lg flex flex-col justify-between items-center bg-gray-unni
                    computer:w-full computer:h-full
                    tablet:w-full tablet:h-full"
    >
      <h1>Création du produit</h1>
      <div className="w-full flex flex-row justify-between p-2">
        <button className={isMenuOne} onClick={handleClickMenuOne}>
          menu
        </button>
        <button className={isMenuTwo} onClick={handleClickMenuTwo}>
          Cocktail
        </button>
        <button className={isMenuThree} onClick={handleClickMenuThree}>
          Ingrédient
        </button>
      </div>
      <hr className="bg-blue-unni h-0.5 rounded-lg mb-3 mt-3 w-4/5" />
      <Box
        className="grid grid-cols-3 grid-row-3 w-full p-2 content-center gap-x-4 gap-y-4"
        component="form"
        sx={{
          "& > :not(style)": {
            width: "auto",
            height: "auto",
            fontFamily: "MontserratAlternates",
          },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          className="col-span-2 w-32"
          id="outlined-basic"
          label="Nom du produit"
          variant="outlined"
          size="small"
        />
        <button className="row-span-3 w-1/2 p-2 border-dashed border-2 border-blue-unni rounded-lg">
          <Image src={"/plus.png"} width={20} height={20}></Image>
          <p>Inserer une image</p>
        </button>
        <TextField id="outlined-basic" label="Prix" size="small" />
        <TextField
          id="outlined-select-currency"
          select
          label="Catégorie"
          value={category}
          onChange={handleChange}
          size="small"
        >
          {categories.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          id="outlined-basic"
          label="Contenance"
          variant="outlined"
          size="small"
        />
        <TextField
          id="outlined-basic"
          label="Quantité"
          variant="outlined"
          size="small"
        />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
            label="Date"
            inputFormat="dd/MM/yyyy"
            value={value}
            onChange={handleChangeDate}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>

        <TextField
          className="col-span-2"
          id="outlined-basic"
          label="Code barre"
          variant="outlined"
          size="small"
        />
      </Box>

      <hr className="bg-blue-unni h-0.5 rounded-lg mb-3 mt-3 w-4/5" />
      <div className="w-full p-2">
        <h4>Ingrédients</h4>
        <div className="flex flex-row w-full justify-around">
          <button className="p-2 h-10 bg-blue-unni rounded-lg w-1/3 text-white">
            Ajouter
          </button>
          <button className="p-2 h-10 bg-white rounded-lg w-1/3 ">
            Ajouter
          </button>
        </div>
      </div>
      <div className="w-full overflow-auto">
        <LineItem />
        <LineItem />
        <LineItem />
        <LineItem />
        <LineItem />
        <LineItem />
        <LineItem />
      </div>
      <button className="text-white font-montserrat bg-blue-unni rounded-lg h-16 w-72 mt-2 mb-2">
        Ajouter
      </button>
    </div>
  );
};

const Menu: NextPage = () => {
  // State
  const [IsClicked, setIsClicked] = useState(true);
  const [doDisplayIngredientList, setDoDisplayIngredientList] = useState(false);

  return (
    <Layout page="Menu">
      {
        <div className="font-montserrat w-full flex flex-col tablet:flex-row tablet:gap-x-3 computer:flex-row computer:gap-x-3 tablet:h-full justify-between overflow-hidden">
          {/* Left-side of layout - Categories and items */}
          <article className="flex flex-wrap justify-evenly space-x-2 tablet:h-full tablet:w-2/3 computer:w-3/4 tablet:overflow-auto computer:overflow-auto">
            <ItemEditDashedComponent />
            <ItemEditComponent />
          </article>
          {/* Right-side of layout - Ticket and Payments info */}
          <aside className="hidden computer:flex computer:flex-row tablet:flex tablet:flex-row">
            {doDisplayIngredientList ? (
              <div className=" rounded-lg w-96 h-full">
                <SelectIngredientComponent />
              </div>
            ) : null}
            <div className="rounded-lg w-96 h-full">
              {/* flex flex-col gap-5 rounded-xl px-5 py-1 tablet:h-full tablet:w-1/3 computer:w-1/4 
                   tablet:overflow-auto computer:overflow-auto' style={{ backgroundColor: '#EFF0F9' }} */}
              <EditMenuComponent
                displayIgredientList={() => setDoDisplayIngredientList(true)}
                hideIgredientList={() => setDoDisplayIngredientList(false)}
              />
            </div>
          </aside>
        </div>
      }
    </Layout>
  );
};
export default Menu;

function buttonStylised():
  | React.MouseEventHandler<HTMLButtonElement>
  | undefined {
  throw new Error("Function not implemented.");
}
