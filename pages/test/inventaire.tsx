import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Layout from '../../components/Layout'
import CategorySelector from '../../components/CategorySelector'
import OptionSelector from '../../components/OptionSelector'
import { Item } from 'semantic-ui-react'

import { TAILWINDCSS_CLASS_TITLE_DEFAULT } from '../../config/css/titles'
/*import ItemSelector from '../components/ItemSelector'*/

/*const INITIAL_STATE_TABS = [
  { name: 'Orders', href: '#waiting', current: true },
  { name: 'Cooking', href: '#cooking', current: false },
  { name: 'To pay', href: '#to_pay', current: false },
  { name: 'Payed', href: '#payed', current: false },
]*/

const INITIAL_STATE_TABS = [
  { name: 'Category #1', href: '#category1', current: true },
  { name: 'Category #2', href: '#category2', current: false }
]

const INITIAL_STATE_OPTIONS = [
  { name: 'Option #1', current: true },
  { name: 'Option #2', current: false },
  { name: 'Option #3', current: false }
]

// list of test items 
const LIST_OF_ITEMS: componentItem[] = [
  { src: '/menu.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/menu.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 713 },
  { src: '/menu.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/menu.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 713 },
  { src: '/menu.png', title: 'CocaCola', contenance: 33, price: 2, quantity: 45 },
  { src: '/menu.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 713 },
  { src: '/menu.png', title: 'Pepsi', contenance: 25, price: 8, quantity: 713 },

]

// Component items/product
interface componentItem {
  src: string;
  title: string;
  contenance: number;
  price: number;
  quantity: number
}

// Component
const createComponent = (newItem: componentItem, index: number) => (
  <div key={index} className="flex items-center gap-2 m-2 p-1 bg-gray-unni border rounded-lg">
    <div className="rounded-full p-2 h-16 w-16 bg-blue-unni flex items-center justify-center">
      <Image src={newItem.src} width={20} height={20}></Image>
    </div>
    <div className='grid content-center space-y-1'>
      <div className='flex justify-center space-y-1 gap-x-2'>
        <h2 className='leading-3'>{newItem.title}</h2>
        <h3 className='leading-3'>{newItem.contenance}cl</h3>
      </div>
      <h4 className='leading-3'>€{newItem.price}</h4>
    </div>
  </div>
)

const Inventaire: NextPage = () => {
  return (
    <Layout page='Inventaire'>
      <div className='w-full flex flex-col tablet:flex-row tablet:gap-x-3 tablet:h-full justify-between'>
        {/* Left-side of layout - Categories and items */}
        <article className='flex flex-col tablet:block tablet:h-full tablet:w-2/3 computer:w-3/4'>
          <CategorySelector tabs={INITIAL_STATE_TABS} />
          { /* <ItemSelector /> */}
          <div className='flex justtify-evenly'>
            {/* entête recherche et scan */}
            <div className='flex w-full gap-4 pt-2'>
              <button className='border rounded-lg p-2'>Rechercher</button>
              <Image src="/code_barre.png" width={50} height={50}></Image>
              <Image src="/plus.png" width={50} height={50}></Image>
            </div>
            <h2>Description</h2>
          </div>
          <section className='flex'>
            <div className='w-1/2'>
              {/* liste des elements du stock */}
              {LIST_OF_ITEMS.map(
                (element: componentItem, index: number) => createComponent(element, index)
              )}
            </div>
            <div className='bg-gray-unni m-2 p-2 w-1/2 rounded-lg'>

            </div>
          </section>
        </article>
        {/* Right-side of layout - Ticket and Payments info */}
        <aside className='tablet:block tablet:h-full tablet:w-1/3 computer:w-1/4 bg-gray-unni'>
          <span className={TAILWINDCSS_CLASS_TITLE_DEFAULT}>Produit #00000</span>
          <section className='flex flex-col bg-blue-unni'>
            {/* <Image src="/beer.png" width={300} height={300}></Image> */}
          </section>
        </aside>
      </div>
    </Layout>
  )
}

export default Inventaire
