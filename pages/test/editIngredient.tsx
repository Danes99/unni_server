import type { NextPage } from "next";
import Layout from "../../components/Layout";
import Image from "next/image";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import { AiOutlineEuroCircle } from "react-icons/ai";

import InputAdornment from "@mui/material/InputAdornment";
import DesktopDatePicker from "@mui/lab/DesktopDatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";

import * as React from "react";
import { map, style } from "d3";
import classNames from "classnames";
import { useState, Fragment } from "react";
import { NextMiddlewareResult } from "next/dist/server/web/types";

const ITEMS_LIST: IIngredient[] = [
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
];

interface IIngredient {
  title: string;
  src: string;
  price: number;
  quantity: number;
}

interface IPropsEditIngredientComponent {
  displayIngredientForm: any;
  hideIngredientForm: any;
}

interface IPropsItemIngredientEditComponent {
  ingredient: IIngredient;
  handleClickEditIcon: () => void;
}

const categories = [
  {
    value: "Boisson",
  },
  {
    value: "Plat",
  },
  {
    value: "Dessert",
  },
  {
    value: "Snack",
  },
];

// Editable Ingredient Item
const ItemIngredientEditComponent = ({
  ingredient,
  handleClickEditIcon,
}: IPropsItemIngredientEditComponent) => (
  <div
    className="border-4 border-blue-unni rounded-lg flex flex-col justify-between items-center space-y-2 bg-gray-unni
                    computer:w-44 computer:h-52 
                    tablet:w-44 tablet:h-52"
  >
    <span className="text-base font-montserrat">{ingredient.title}</span>
    <div
      className="rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
                        tablet:border-4 tablet:h-20 tablet:w-20
                        computer:border-4 computer:h-20 computer:w-20"
    >
      <Image src={ingredient.src} width={30} height={30}></Image>
    </div>
    <div className="space-x-2 text-xs">
      <span>{ingredient.price}€</span>
      <span>.</span>
      <span>{ingredient.quantity} produit(s)</span>
    </div>
    <button
      onClick={handleClickEditIcon}
      className="h-12 w-full bg-blue-secondary space-x-2 text-sm flex items-center justify-center"
    >
      <span>
        <Image src={"/edit.png"} width={20} height={20}></Image>
      </span>
      <span className="text-white">Modifier</span>
    </button>
  </div>
);

// item for edit dotted component
const ItemEditDashedComponent = () => (
  <button
    className="border-4 border-dashed border-blue-unni rounded-lg
        flex flex-col justify-center items-center bg-gray-unni
        computer:w-44 computer:h-52 
        tablet:w-44 tablet:h-52"
  >
    <div className="h-20 w-20 flex flex-col items-center justify-center">
      <Image src={"/plus.png"} width={40} height={40}></Image>
    </div>
    <p>Ajouter</p>
  </button>
);

// right side for edit ingredient
const EditIngredientComponent = () => {
  const [category, setCategory] = React.useState("Plat");
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCategory(event.target.value);
  };
  const [value, setValue] = React.useState<Date | null>(new Date());

  const handleChangeDate = (newValue: Date | null) => {
    setValue(newValue);
  };
  return (
    <div
      className="rounded-lg flex flex-col justify-between items-center bg-gray-unni
                computer:w-full computer:h-full
                tablet:w-full tablet:h-full"
    >
      <h1 className="font-montserrat">Modification de l'ingrédient</h1>
      <Box
        className="grid grid-cols-3 grid-row-3 w-full p-2 content-center gap-x-4 gap-y-8"
        component="form"
        sx={{
          "& > :not(style)": {
            width: "auto",
            height: "auto",
            fontFamily: "MontserratAlternates",
          },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          className="col-span-2 w-32"
          id="outlined-basic"
          label="Nom du produit"
          variant="outlined"
          size="medium"
        />
        <button className="row-span-3 w-1/2 p-2 border-dashed border-2 border-blue-unni rounded-lg">
          <Image src={"/plus.png"} width={20} height={20}></Image>
          <p>Inserer une image</p>
        </button>
        <TextField id="outlined-basic" label="Prix" size="medium" />
        <LocalizationProvider dateAdapter={AdapterDateFns} size="medium">
          <DesktopDatePicker
            label="Date"
            inputFormat="dd/MM/yyyy"
            value={value}
            onChange={handleChangeDate}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <TextField
          id="outlined-basic"
          label="Contenance"
          variant="outlined"
          size="medium"
        />
        <TextField
          id="outlined-basic"
          label="Quantité"
          variant="outlined"
          size="medium"
        />

        {/* <TextField
          className="col-span-2"
          id="outlined-basic"
          label="Code barre"
          variant="outlined"
          size="medium"
        /> */}
      </Box>
      <div className="flex flex-col w-full justify-cente items-center">
        <hr className="bg-black h-0.5 rounded-lg mb-3 mt-3 w-4/5" />
        <div className="flex flex-row justify-center w-full space-x-2">
          <button className="text-red-unni font-montserrat bg-white border border-red-unni rounded-lg h-12 w-1/3 mt-2 mb-2 hover:bg-red-unni hover:text-white">
            Supprimer
          </button>
          <button className="text-blue-unni font-montserrat bg-white border border-blue-unni rounded-lg h-12 w-1/3 mt-2 mb-2 hover:bg-blue-unni hover:text-white">
            Enregistrer
          </button>
        </div>
      </div>
    </div>
  );
};
// right side for add ingredient
const AddIngredientComponent = () => {
  return <div></div>;
};

const INITIAL_STATE_INGREDIENT_TO_EDIT: number | any = null;

const Menu: NextPage = () => {
  // State
  const [IsClicked, setIsClicked] = useState(true);
  //   const [doDisplayIngredientForm, setDoDisplayIngredientForm] = useState(false);
  const [ingredientToEdit, setIngredientToEdit] = useState(
    INITIAL_STATE_INGREDIENT_TO_EDIT
  );

  const doDisplayIngredientForm = Number.isInteger(ingredientToEdit);

  const handleClickOnItem = (index: number) => {
    setIngredientToEdit(ingredientToEdit === index ? null : index);
  };

  return (
    <Layout page="Ingredients">
      {
        <div className="hidden font-montserrat w-full tablet:flex computer:flex tablet:h-full tablet:justify-between computer:justify-between overflow-hidden">
          {/* list of all ingredients in edit mode */}
          <article className="flex flex-wrap justify-evenly h-full overflow-auto gap-8">
            <ItemEditDashedComponent />
            {ITEMS_LIST.map((element: IIngredient, index: number) => (
              <ItemIngredientEditComponent
                key={index}
                ingredient={element}
                handleClickEditIcon={() => handleClickOnItem(index)}
              />
            ))}
          </article>
          {doDisplayIngredientForm ? (
            <aside className="rounded-lg w-2/3 h-full bg-green-600">
              <EditIngredientComponent />
            </aside>
          ) : null}
        </div>
      }
    </Layout>
  );
};
export default Menu;
