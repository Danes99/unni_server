import type {NextPage} from 'next'
import Layout from '../../components/Layout'
// import "./calendar.css"

import { DateRange, DateRangePicker } from 'react-date-range';
import { render } from '@headlessui/react/dist/utils/render';

import React, { useState } from "react";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import { Calendar }  from "react-modern-calendar-datepicker";
import type { DayRange }  from "react-modern-calendar-datepicker";
import DatePicker from 'react-modern-calendar-datepicker';
// import {useState} from 'react'

// const [state, setState] = useState([
//     {
//       startDate: new Date(),
//       endDate: null,
//       key: 'selection'
//     } 
//   ]);

// function handleSelect(ranges: any) {
//     throw new Error('Function not implemented.');
// }

const defaultFrom = {
  year: (new Date).getFullYear(),
  month: (new Date).getUTCMonth()+1,
  day: (new Date).getDate(),
};

const defaultValue: DayRange = {
  from: defaultFrom,
  to: defaultFrom,
};

/* const CalendarItem = ()=>{
  const [selectedDayRange, setSelectedDayRange] = useState(
    defaultValue
  );
  return(
  <Calendar
          value={selectedDayRange}
          onChange={setSelectedDayRange}
          colorPrimary="#343796"
          colorPrimaryLight="#EBEBF4"
          calendarClassName="responsive-calendar" // added this
          // calendarClassName="font-size:20px"
          />)
  } */

const CalendarItem: NextPage = () => {

  const [selectedDayRange, setSelectedDayRange] = useState(
    defaultValue
  );

  return (
    <Layout page='Calendar'>
       <div className='font-montserrat w-full flex flex-col tablet:flex-row tablet:gap-x-3 tablet:h-full justify-between overflow-hidden'>
         <article className='flex flex-col items-center self-center tablet:h-full tablet:w-2/3 computer:w-3/4 tablet:overflow-auto computer:overflow-auto'>
           {/* left part  */}
           <Calendar
          value={selectedDayRange}
          onChange={setSelectedDayRange}
          colorPrimary="#343796"
          colorPrimaryLight="#EBEBF4"
          calendarClassName="responsive-calendar" // added this
          // calendarClassName="font-size:20px"
          />
         </article>
         <aside className='hidden flex flex-col gap-5 rounded-xl px-5 py-1 tablet:block computer:block tablet:h-full tablet:w-1/3 computer:w-1/4 tablet:overflow-auto computer:overflow-auto' style={{ backgroundColor: '#EFF0F9' }}>
           {/* right part */}

         </aside>
       </div>
    </Layout>
  )
}

export default CalendarItem

// https://kiarash-z.github.io/react-modern-calendar-datepicker/docs/getting-started
  
