import type { NextPage } from "next";
import { useState, Fragment } from "react";
import Head from "next/head";
import Image from "next/image";
import Layout from "../../components/Layout";
//import CategorySelector from '../../components/CategorySelector'
import OptionSelector from "../../components/OptionSelector";
import { Item } from "semantic-ui-react";

import { TAILWINDCSS_CLASS_TITLE_DEFAULT } from "../../config/css/titles";
import useLongPress from "../../utils/useLongPress";

import { FiSettings } from "react-icons/fi";

import * as React from "react";
import Box from "@mui/material/Box";
import Badge from "@mui/material/Badge";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";
//import AddIcon from '@mui/icons-material/AddIcon';
// import RemoveIcon from '@mui/icons-material/RemoveIcon';
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { table } from "console";
import { active } from "d3";
import classNames from "classnames";

const shapeStyles = { bgcolor: "primary.main", width: 40, height: 40 };
const shapeCircleStyles = { borderRadius: "50%" };
const rectangle = <Box component="span" sx={shapeStyles} />;
const circle = (
  <Box component="span" sx={{ ...shapeStyles, ...shapeCircleStyles }} />
);

/*import ItemSelector from '../components/ItemSelector'*/

/*const INITIAL_STATE_TABS = [
  { name: 'Orders', href: '#waiting', current: true },
  { name: 'Cooking', href: '#cooking', current: false },
  { name: 'To pay', href: '#to_pay', current: false },
  { name: 'Payed', href: '#payed', current: false },
]*/

const INITIAL_STATE_TABS = [
  {
    title: "Category #1",
    id: 1,
    htmlid: "category1",
    href: "#category1",
    current: true,
  },
  {
    title: "Category #2",
    id: 2,
    htmlid: "category2",
    href: "#category2",
    current: false,
  },
  {
    title: "Category #3",
    id: 3,
    htmlid: "category3",
    href: "#category3",
    current: false,
  },
  {
    title: "Category #4",
    id: 4,
    htmlid: "category4",
    href: "#category4",
    current: false,
  },
];

const INITIAL_STATE_OPTIONS = [
  { name: "Option #1", current: true },
  { name: "Option #2", current: false },
  { name: "Option #3", current: false },
  { name: "Option #4", current: false },
];

const TEST_CART_ITEMS: ICartComponent[] = [
  {
    product_id: 1,
    src: "/beer.png",
    title: "CocaCola",
    quantity: 1,
    unitprice: 2,
    notes: "Sans sucre",
  },
  {
    product_id: 2,
    src: "/beer.png",
    title: "CocaCola",
    quantity: 1,
    unitprice: 2,
    notes: "Sans sucre",
  },
];

// list of test items
const TEST_ADD_ITEMS: componentItem[] = [
  {
    src: "/beer.png",
    title: "CocaCola",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 1,
  },
  {
    src: "/beer.png",
    title: "Pepsi",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 2,
  },
  {
    src: "/beer.png",
    title: "Fanta",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 3,
  },
  {
    src: "/beer.png",
    title: "Sprite",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 4,
  },
  {
    src: "/beer.png",
    title: "CocaCola",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 1,
  },
  {
    src: "/beer.png",
    title: "Pepsi",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 2,
  },
  {
    src: "/beer.png",
    title: "Fanta",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 3,
  },
  {
    src: "/beer.png",
    title: "Sprite",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 4,
  },
  {
    src: "/beer.png",
    title: "CocaCola",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 1,
  },
  {
    src: "/beer.png",
    title: "Pepsi",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 2,
  },
  {
    src: "/beer.png",
    title: "Fanta",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 3,
  },
  {
    src: "/beer.png",
    title: "Sprite",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 4,
  },
  {
    src: "/beer.png",
    title: "CocaCola",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 1,
  },
  {
    src: "/beer.png",
    title: "Pepsi",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 2,
  },
  {
    src: "/beer.png",
    title: "Fanta",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 3,
  },
  {
    src: "/beer.png",
    title: "Sprite",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 4,
  },
  {
    src: "/beer.png",
    title: "CocaCola",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 1,
  },
  {
    src: "/beer.png",
    title: "Pepsi",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 2,
  },
  {
    src: "/beer.png",
    title: "Fanta",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 3,
  },
  {
    src: "/beer.png",
    title: "Sprite",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 4,
  },
  {
    src: "/beer.png",
    title: "CocaCola",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 1,
  },
  {
    src: "/beer.png",
    title: "Pepsi",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 2,
  },
  {
    src: "/beer.png",
    title: "Fanta",
    contenance: 33,
    unit: "cl",
    price: 2,
    quantity: 45,
    categoryid: 3,
  },
  {
    src: "/beer.png",
    title: "Sprite",
    contenance: 25,
    unit: "cl",
    price: 8,
    quantity: 713,
    categoryid: 4,
  },
];

// Component items/product
interface ICartComponent {
  product_id: number;
  src: string;
  title: string;
  unitprice: number;
  quantity: number;
  notes: string;
}

interface componentItem {
  src: string;
  title: string;
  contenance: number;
  unit: string;
  price: number;
  quantity: number;
  categoryid: number;
}

interface ICategoryComponent {
  title: string;
  id: number;
  htmlid: string;
  href: string;
  current: boolean;
}

// Component
const createComponentCart = (CartItem: ICartComponent, index: number) => {
  const [quantity, setQuantity] = React.useState(1);
  const [notes, setData] = React.useState("");

  const addQuantity = () => {
    if (quantity < 99) {
      setQuantity(quantity + 1);
      CartItem.quantity = quantity + 1;
    }
  };

  const removeItemFromCart = () => { };

  const remQuantity = () => {
    if (quantity != 1) {
      setQuantity(quantity - 1);
      CartItem.quantity = quantity - 1;
    }
  };

  const handleNoteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setData(e.target.value);
    CartItem.notes = e.target.value;
  };

  return (
    <article key={index} className="flex flex-col w-full gap-1">
      <div className="flex flex-row gap-2 items-center justify-between mr-1">
        {/* Product informations */}
        <section className="flex flex-row gap-2 w-1/2 flex-wrap">
          <div className="flex justify-center content-center rounded-full h-10 w-10 bg-blue-unni">
            <Image src={CartItem.src} width={30} height={30}></Image>
          </div>
          <div className="inline-flex flex-col grow">
            <span>{CartItem.title}</span>
            <span className="text-xs">{CartItem.unitprice.toFixed(2)}€</span>
          </div>
        </section>

        {/* Quantity */}
        <section className="flex flex-row gap-1 w-1/4 flex-wrap">
          <div className="flex flex-row h-10 w-10 rounded-lg relative bg-transparent mt-1">
            <button
              onClick={remQuantity}
              data-action="decrement"
              className=" bg-gray-200 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-content rounded-l cursor-pointer outline-none"
            >
              <span className="m-auto text-2xl font-thin">−</span>
            </button>
            <input
              onChange={() => { }}
              type="text"
              className="form-control text-center font-semibold text-md w-12 text-gray-700 bg-gray-200 bg-clip-padding border border-solid border-gray-300 transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              value={CartItem.quantity}
            ></input>
            <button
              onClick={addQuantity}
              data-action="increment"
              className="bg-gray-200 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-content rounded-r cursor-pointer"
            >
              <span className="m-auto text-2xl font-thin">+</span>
            </button>
          </div>
        </section>

        {/* Price */}

        <section className="text-center mx-1 w-1/4">
          <span className="font-bold">
            {(CartItem.unitprice * CartItem.quantity).toFixed(2)}€
          </span>
        </section>
      </div>
      <div className="w-full flex flex-row gap-2">
        <input
          onChange={handleNoteChange}
          value={notes}
          type="text"
          className="form-control w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-gray-200 bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
          placeholder="Note de commande"
        />
        <button
          onClick={(event) => removeItemFromCart()}
          className="rounded-l rounded-r w-10 h-10 bg-gray-200 border-2 border-red-500 font-bold text-xs leading-tight hover:bg-red-500 transition duration-150 ease-in-out"
        >
          <div className="h-auto w-auto flex justify-center">
            <Image src="/remove.png" width={20} height={20}></Image>
          </div>
        </button>
      </div>
    </article>
  );
};

/* Category Selector */

interface IProps {
  tabs: ICategoryComponent[];
}

const SelectorCategory = ({ tabs }: IProps) => {
  // State
  const [, setDoRefresh] = useState(true);

  const updateTabHandler = (index: number) => {
    tabs.forEach((element: ICategoryComponent) => {
      element.current = false;
    });

    tabs[index].current = true;

    setDoRefresh((current) => !current);
  };

  return (
    <section className="border-b border-gray-200 rounded-full">
      <nav
        className="font-montserrat flex space-x-8 justify-evenly"
        aria-label="Tabs"
      >
        {tabs.map((tab: ICategoryComponent, index: number) => (
          <a
            key={tab.title}
            href={tab.href}
            className={classNames(
              tab.current
                ? "border-blue-unni border-b-4 text-blue-unni"
                : "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300",
              "whitespace-nowrap py-4 px-1 border-b-4 font-medium text-sm flex-auto text-center"
            )}
            aria-current={tab.current ? "page" : undefined}
            onClick={(event) => updateTabHandler(index)}
          >
            {tab.title}
          </a>
        ))}
        <SidebarIcon icon={<FiSettings size="30" />} />
      </nav>
      {/* <ItemSelector /> */}
      <section className="h-full">
        {tabs.map((tab: ICategoryComponent, index: number) => (
          <div
            key={index}
            id={tab.htmlid}
            className={classNames(
              tab.current ? "show active" : "hidden",
              "flex flex-wrap gap-2 justify-start p-2"
            )}
          >
            {TEST_ADD_ITEMS.map((element: componentItem, index: number) => {
              if (element.categoryid == tab.id) {
                return createComponentItem(element, index);
              }
            })}
          </div>
        ))}
      </section>
    </section>
  );
};

// Component
const createComponentItem = (newItem: componentItem, index: number) => {
  const [count, setCount] = React.useState(0);
  const [invisible, setInvisible] = React.useState(false);

  const handleBadgeVisibility = () => {
    setInvisible(!invisible);
  };

  const onLongPress = () => {
    // console.log('longpress is triggered');
    setCount(Math.max(count - 1, 0));
    if (count <= 0) {
      setInvisible(invisible);
    }
  };

  const onClick = () => {
    // console.log('click is triggered')
    setCount(count + 1);
    // setInvisible(!invisible);
  };

  const defaultOptions = {
    shouldPreventDefault: true,
    delay: 500,
  };
  const longPressEvent = useLongPress(onLongPress, onClick, defaultOptions);

  return (
    <Badge
      color="error"
      badgeContent={count}
      max={99}
      className="mt-4"
      invisible={invisible}
    >
      <div key={index} className='border-2 border-blue-unni rounded-lg p-2 flex flex-col justify-between items-center w-36 h-36 bg-gray-unni
                      computer:w-44 computer:h-44 computer:border-4 computer:space-y-2
                      tablet:w-44 tablet-44 tablet:border-4 tablet:space-y-2'>
        <div className='text-xs
                          tablet:text-base
                          computer:text-base'>
          {newItem.title}
        </div>
        <div className='rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
                          tablet:border-4 tablet:h-20 tablet:w-20
                          computer:border-4 computer:h-20 computer:w-20'>
          <Image src={newItem.src} width={25} height={25}></Image>
        </div>
        <div className='space-x-2 text-xs'>
          <span>{newItem.price.toFixed(2)}€</span>
          <span>-</span>
          <span>{newItem.quantity} produit(s)</span>
        </div>
      </div>
    </Badge>
  );
};

const SidebarIcon = ({ icon }: any) => (
  <div className="sidebar-icon">
    {icon}
  </div>
)


const Restaurant: NextPage = () => {
  return (
    <Layout page="Restaurant">
      <div className="font-montserrat w-full flex flex-col tablet:flex-row tablet:gap-x-3 tablet:h-full justify-between overflow-hidden">
        {/* Left-side of layout - Categories and items */}
        <article className="flex flex-col tablet:h-full tablet:w-2/3 computer:w-3/4 tablet:overflow-auto computer:overflow-auto">
          <div className="flex justify-between tablet:hidden computer:hidden gap-8">
            <button className="border rounded-lg w-full flex justify-between p-2">
              Rechecher
              <Image src={"/search.png"} width={20} height={20}></Image>
            </button>
            <div className="flex self-center">
              <Image src={"/cart.png"} width={30} height={30}></Image>
            </div>
          </div>
          <SelectorCategory tabs={INITIAL_STATE_TABS} />
        </article>
        {/* Right-side of layout - Ticket and Payments info */}
        <aside
          className="hidden flex-col gap-5 rounded-xl px-5 py-1 tablet:block computer:block tablet:h-full tablet:w-1/3 computer:w-1/4 tablet:overflow-auto computer:overflow-auto"
          style={{ backgroundColor: "#EFF0F9" }}
        >
          {/* Order Number section */}
          <section className="mt-5 justify-self-center">
            <span className="w-full text-2xl text-center text-gray-800">
              Order #00000
            </span>
          </section>
          <section className="mt-2">
            <OptionSelector options={INITIAL_STATE_OPTIONS} />
          </section>

          <section className="mt-2">
            <div className="flex flex-row justify-items-between">
              <span className="w-3/5 text-2xl text-left text-gray-800 grow">
                Products
              </span>
              <span className="w-1/5 text-2xl text-left text-gray-800 shrink">
                QT
              </span>
              <span className="w-1/5 text-2xl text-left text-gray-800">
                Price
              </span>
            </div>
            <hr className="bg-gray-500 h-0.5 rounded-lg my-3" />
            <section className="flex flex-col w-full gap-5">
              {TEST_CART_ITEMS.map((element: ICartComponent, index: number) =>
                createComponentCart(element, index)
              )}
            </section>
          </section>

          <section className="self-end w-full inline-flex flex-col">
            <hr className="bg-gray-500 h-0.5 rounded-lg mb-3" />
            <div className="flex flew-row max-h-max justify-between mx-2">
              <span className="text-gray-600">Subtotal</span>
              <span className="text-align-right justify-self-end text-blue-unni">
                {TEST_CART_ITEMS.reduce(
                  (sum, element) =>
                    (sum += element.unitprice * element.quantity), 0).toFixed(2)
                }€
              </span>
            </div>
            <div className="flex flew-row max-h-max justify-between mx-2 py-2">
              <span className="text-gray-600">TVA 20%</span>
              <span className="text-align-right text-blue-unni">
                {(
                  TEST_CART_ITEMS.reduce(
                    (sum, element) =>
                      (sum += element.unitprice * element.quantity), 0) * 0.2).toFixed(2)
                }€
              </span>
            </div>
            <button
              type="button"
              className="font-montserrat rounded-md px-1 py-1.5 shadow-xl h-10 bg-blue-unni text-white border-2 font-bold text-xs leading-tight hover:bg-indigo-800 focus:bg-blue-unni focus:text-white focus:outline-none focus:ring-0 active:bg-blue-unni transition duration-150 ease-in-out"
            >
              Continue with payment
            </button>
          </section>
        </aside>
      </div>
    </Layout>
  );
};

export default Restaurant;
