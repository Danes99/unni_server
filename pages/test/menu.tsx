import type { NextPage } from "next";
import Layout from "../../components/Layout";
import Image from "next/image";

import * as React from "react";
import { map } from "d3";

import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

//Dropdown button
function BasicSelect(props: any) {
  const [Categorie, setCategorie] = React.useState("");

  const handleChange = (event: any) => {
    setCategorie(event.target.value);
  };

  const listItems = props.items.map((item: any) => (
    <MenuItem value={item} sx={{ fontSize: 16 }}>
      {item}
    </MenuItem>
  ));

  return (
    <div>
      <FormControl fullWidth>
        <Select
          sx={{
            fontSize: 12,
            height: 28,
            fontWeight: "bold",
          }}
          onChange={handleChange}
        >
          {listItems}
        </Select>
      </FormControl>
    </div>
  );
}

const ITEMS_LIST: componentSelector[] = [
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
  { src: "/legume.png", title: "cocaCola", price: 2.3, quantity: 23 },
];

interface componentSelector {
  title: string;
  src: string;
  price: number;
  quantity: number;
}
// right side empty cart component
const EmptyComponent = () => (
  <div className="h-full flex flex-col items-center gap-y-16">
    <h1>Nouvelle commande</h1>
    <h3>Selectionnez un article</h3>
    <div className="w-40 h-40 flex flex-col justify-center items-center">
      <Image src={"/cart.png"} width={180} height={180}></Image>
    </div>
  </div>
);

// item for phone/tablet component
const ItemComponent = (item: componentSelector, index: number) => (
  <div
    key={index}
    className="border-2 border-blue-unni rounded-lg p-2 flex flex-col justify-between items-center w-36 h-36 bg-gray-unni
                    computer:w-44 computer:h-44 computer:border-4 computer:space-y-2
                    tablet:w-44 tablet-44 tablet:border-4 tablet:space-y-2"
  >
    <div
      className="text-xs
                        tablet:text-base
                        computer:text-base"
    >
      {item.title}
    </div>
    <div
      className="rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
                        tablet:border-4 tablet:h-20 tablet:w-20
                        computer:border-4 computer:h-20 computer:w-20"
    >
      <Image src={item.src} width={25} height={25}></Image>
    </div>
    <div className="space-x-2 text-xs">
      <span>{item.price}€</span>
      <span>.</span>
      <span>{item.quantity} produit(s)</span>
    </div>
  </div>
);

// item for edit component
const ItemEditComponent = () => (
  <div
    className="border-4 border-blue-unni rounded-lg flex flex-col justify-between items-center space-y-2 bg-gray-unni
                    computer:w-44 computer:h-52 
                    tablet:w-44 tablet:h-52"
  >
    <span className="text-base font-montserrat">Canette CocaCola</span>
    <div
      className="rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
                        tablet:border-4 tablet:h-20 tablet:w-20
                        computer:border-4 computer:h-20 computer:w-20"
    >
      <Image src={"/cart.png"} width={30} height={30}></Image>
    </div>
    <div className="space-x-2 text-xs">
      <span>2,3€</span>
      <span>.</span>
      <span>45 produit(s)</span>
    </div>
    <button className="h-12 w-full bg-blue-secondary space-x-2 text-sm flex items-center justify-center">
      <span>
        <Image src={"/edit.png"} width={20} height={20}></Image>
      </span>
      <span className="text-white">Modifier</span>
    </button>
  </div>
);

// item for edit dotted component
const ItemEditDashedComponent = () => (
  <button
    className="border-4 border-dashed border-blue-unni rounded-lg flex flex-col justify-center items-center bg-gray-unni
                    computer:w-44 computer:h-52 
                    tablet:w-44 tablet:h-52"
  >
    <div className="h-20 w-20 flex flex-col items-center justify-center">
      <Image src={"/plus.png"} width={40} height={40}></Image>
    </div>
    <p>Ajouter</p>
  </button>
);

// Creation of menu, add selected ingredient component
const SelectIngredientComponent = () => (
  <div
    className="rounded-lg flex flex-col justify-between items-center bg-gray-unni
                    computer:w-96 computer:h-full 
                    tablet:w-96 tablet:h-full"
  >
    <h1>Ingrédients</h1>
    <div className="h-10 w-72 bg-white rounded-lg flex flex-row justify-around mb-2">
      <input placeholder="Rechercher"></input>
      <div className="h-10 w-10 flex flex-col items-center justify-center">
        <Image src={"/search.png"} width={20} height={20}></Image>
      </div>
    </div>
    <div className="flex flex-wrap  justify-center gap-y-1 gap-x-2 overflow-auto h-full mt-2">
      {ITEMS_LIST.map((element: componentSelector, index: number) =>
        ItemComponent(element, index)
      )}
    </div>
    <button className="text-white font-montserrat bg-blue-unni rounded-lg h-12 w-72 mb-2 mt-2">
      Ajouter
    </button>
  </div>
);

// item in line
const LineItem = () => (
  <div className="flex flex-row h-12 items-center justify-around">
    <div className="rounded-full border-2 border-blue-unni bg-white h-8 w-8 flex flex-row justify-center items-center">
      <Image src={"/beer.png"} width={20} height={20}></Image>
    </div>
    <span>CocaCola 33cl</span>
    <div className="rounded-lg h-8 w-8 border-blue-unni bg-white border-2 flex flex-row items-center justify-center">
      2
    </div>
    <div className="rounded-lg h-8 w-8 border-red-600 bg-white border-2 flex flex-row items-center justify-center">
      <Image src={"/remove.png"} width={20} height={20}></Image>
    </div>
  </div>
);

// edit menu / drinks and ingredients
const EditMenuComponent = () => (
  <div
    className="rounded-lg flex flex-col justify-between items-center bg-gray-unni
                    computer:w-full computer:h-full 
                    tablet:w-full tablet:h-full"
  >
    <h1>Création du produit</h1>
    <div className="w-full flex flex-row justify-between p-2">
      <button className="bg-blue-unni rounded-lg p-2 w-28 text-white font-montserrat">
        menu
      </button>
      <button className="bg-white rounded-lg p-2 w-28 font-montserrat">
        Cocktail
      </button>
      <button className="bg-white rounded-lg p-2 w-28 font-montserrat">
        Ingrédient
      </button>
    </div>
    <div className="w-full p-2 ">
      <h4 className="font-montserrat">Nom du produit</h4>
      <input
        placeholder="CocaCola"
        className="bg-blue-secondary text-white w-full h-8 rounded-lg p-2"
      ></input>
    </div>
    <div className="flex flex-row w-full p-2">
      <div className="flex flex-col">
        <div className="flex flex-row">
          <div className="w-1/2 p-2">
            <h4 className="font-montserrat">Prix (€)</h4>
            <input
              placeholder="3.2"
              className="bg-blue-secondary text-white w-full h-8 rounded-lg p-2"
            ></input>
          </div>
          <div className="w-1/2 p-2 ">
            <h4 className="font-montserrat">Catégorie</h4>
            <BasicSelect items={["boisson", "plat", "dessert"]} />
          </div>
        </div>
        <div className="flex flex-row">
          <div className="w-1/2 p-2 ">
            <h4 className="font-montserrat">Contenence</h4>
            <input
              placeholder="cl"
              className="bg-blue-secondary text-white w-full h-8 rounded-lg p-2"
            ></input>
          </div>
          <div className="w-1/2 p-2 ">
            <h4 className="font-montserrat">Quantité</h4>
            <input
              placeholder="25"
              className="bg-blue-secondary text-white w-full h-8 rounded-lg p-2"
            ></input>
          </div>
        </div>
      </div>
      <button className="w-1/2 p-2 border-dashed border-2 border-blue-unni rounded-lg">
        <Image src={"/plus.png"} width={20} height={20}></Image>
        <p>Inserer une image</p>
      </button>
    </div>
    <hr className="bg-blue-unni h-0.5 rounded-lg mb-3 w-4/5" />
    <div className="w-full p-2">
      <h4>Ingrédients</h4>
      <div className="flex flex-row w-full justify-around">
        <button className="p-2 h-10 bg-blue-unni rounded-lg w-1/3 text-white">
          Ajouter
        </button>
        <button className="p-2 h-10 bg-white rounded-lg w-1/3 ">Ajouter</button>
      </div>
    </div>
    <div className="w-full overflow-auto">
      <LineItem />
      <LineItem />
      <LineItem />
      <LineItem />
      <LineItem />
      <LineItem />
      <LineItem />
    </div>
    <button className="text-white font-montserrat bg-blue-unni rounded-lg h-16 w-72 mt-2 mb-2">
      Ajouter
    </button>
  </div>
);

const Menu: NextPage = () => {
  return (
    <Layout page="Menu">
      <div className="font-montserrat w-full flex flex-col tablet:flex-row tablet:gap-x-3 computer:flex-row computer:gap-x-3 tablet:h-full justify-between overflow-hidden">
        {/* Left-side of layout - Categories and items */}
        <article className="flex flex-wrap justify-evenly space-x-2 tablet:h-full tablet:w-2/3 computer:w-3/4 tablet:overflow-auto computer:overflow-auto">
          <ItemEditDashedComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          <ItemEditComponent />
          {/* <ItemComponent /> */}
        </article>
        {/* Right-side of layout - Ticket and Payments info */}
        <aside className="hidden computer:flex computer:flex-row tablet:flex tablet:flex-row">
          <div className=" rounded-lg w-96 h-full">
            <SelectIngredientComponent />
          </div>
          <div className="rounded-lg w-96 h-full">
            {/* flex flex-col gap-5 rounded-xl px-5 py-1 tablet:h-full tablet:w-1/3 computer:w-1/4 
                   tablet:overflow-auto computer:overflow-auto' style={{ backgroundColor: '#EFF0F9' }} */}
            <EditMenuComponent />
          </div>
        </aside>
      </div>
    </Layout>
  );
};

export default Menu;
