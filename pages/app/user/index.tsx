// Import pre-installed modules
import type { NextPage } from 'next'
import Link from 'next/link'
import Image from 'next/image'
import { GetServerSideProps } from 'next'
import { NextResponse } from 'next/server'

// Import downloaded modules
import { Icon } from 'semantic-ui-react'
import { GraphQLClient, gql } from 'graphql-request'

// Import custom components
import Layout from '../../../components/Layout'

// Import config
import { API_ENDPOINT } from '../../../config/api'
// import LANGUAGES from './index.lang.json'

interface IUserProps {
  role?: string
  name?: string
  email?: string
  createdAt?: string
  updatedAt?: string
}

interface IProps {
  user: IUserProps
}

const readUserSelf = gql`
  query ReadUserSelf {
    readUserSelf {
      id
      createdAt
      updatedAt
      name
      email
      role
    }
  }
`

export const getServerSideProps: GetServerSideProps = async (context: any) => {

  const jwt = context.req.cookies?.jwt || null

  try {

    const client = new GraphQLClient(
      API_ENDPOINT,
      { headers: { authorization: jwt } }
    )

    const data = await client.request(readUserSelf)

    return { props: { user: data.readUserSelf } }

  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

const Page: NextPage<IProps> = ({ user }) => {
  return (
    <Layout page='User'>

      <div className="flex justify-center">

        <div className="w-full md:w-3/5 bg-white p-6">
          <div className="flex items-center border-b py-4">
            <div>
              <i className="fas fa-chevron-left text-lg"></i>
            </div>
            <div className="flex-1">
              <p className="text-center">{user.name}</p>
            </div>
            <div>
              <i className="fas fa-pencil-alt"></i>
            </div>
            <div className="ml-4">
              <i className="fas fa-search"></i>
            </div>
          </div>
          <div className="mt-6">
            <div className="h-48 overflow-hidden rounded-tl-lg rounded-tr-lg">
              {/* <Image
                className="rounded-tl-xl rounded-tr-xl"
                src="https://pbs.twimg.com/profile_banners/607109926/1622322022/1500x500"
                alt="Image"
              /> */}
            </div>
          </div>
          <div className="-mt-24">
            <div className="flex justify-center rounded-full">
              {/* <Image
                className="w-32 rounded-full"
                src="https://scontent-iad3-2.xx.fbcdn.net/v/t1.6435-9/195080787_3069500069997336_8089110405115182844_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=-mLpFQvyElMAX9XI6Ca&_nc_ht=scontent-iad3-2.xx&oh=5437bb06a33c393b57300424d4704102&oe=6127FF5F"
                alt="Image"
              /> */}
            </div>
          </div>
          <div className="mt-2 text-lg">
            <p className="text-center"><span className="font-bold">{user.name}</span> ({user.email})</p>
          </div>
          <div className="mt-2 flex justify-center text-lg">
            <p className="text-center w-5/6">Role: <span className="font-bold">{user.role}</span></p>
          </div>
          <div className="flex pb-4 mt-4 items-center border-b">
            <button className="py-2 rounded flex-1 bg-blue-500 text-white">
              <i className="fas fa-plus-circle"></i> Add to Story
            </button>
            <button className="bg-gray-200 py-2 px-4 rounded ml-2" />
            <i className="fas fa-ellipsis-h"></i>
          </div>

          <div>
            <div className="flex mt-2 items-center">
              <div className="text-gray-400">
                <Icon size='big' name='mail' />
              </div>

              <div className="text-lg ml-3">
                <p><span className="font-bold">{user.email}</span></p>
              </div>
            </div>
          </div>

          <div className="flex mt-2 items-center">
            <div className="text-gray-400">
              <Icon size='big' name='calendar alternate' />
            </div>

            <div className="text-lg ml-3">
              <p>Joined<span className="font-bold">{user.createdAt}</span></p>
            </div>
          </div>

          <div className="flex mt-2 items-center">
            <div className="text-gray-400">
              <Icon size='big' name='calendar alternate outline' />
            </div>

            <div className="text-lg ml-3">
              <p>Updated at <span className="font-bold">{user.updatedAt}</span></p>
            </div>
          </div>

        </div>
      </div>

    </Layout>
  )
}

export default Page
