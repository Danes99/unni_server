import { NextFetchEvent, NextRequest, NextResponse } from 'next/server'

// Import downloaded modules
import { gql } from 'graphql-request'

const queryReadUserID = gql`
query Query { 
  readUserSelf { 
    id 
    } 
  }
`

// Import config
import { API_ENDPOINT } from '../../config/api'
import { ROUTE_USER_LOGIN } from '../../config/routes'

export default async function middleware(req: NextRequest, event: NextFetchEvent) {

  try {

    // Retrieve token from cookieStorage
    const token = req.cookies?.jwt
    if (!token) throw new Error('Token null')

    const response = await fetch(
      API_ENDPOINT,
      {
        method: 'POST',
        body: JSON.stringify({
          query: queryReadUserID,
        }),
        headers: {
          'Content-Type': 'application/json',
          'authorization':
            token
        }
      }
    )

    const data = await response.json()
    if (!data.data?.readUserSelf?.id) throw new Error('User not logged in')

    return

  } catch (error) {
    console.error(error)
    return NextResponse.redirect(
      new URL(ROUTE_USER_LOGIN, req.url)
    )
  }
}