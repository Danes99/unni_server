// Import framework modules
import Image from 'next/image'
import { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'

// Import downloaded modules
import Cookies from 'js-cookie'
import { GraphQLClient, gql } from 'graphql-request'

// Import downloaded modules: Material UI
import Box from '@mui/material/Box'
import MenuItem from '@mui/material/MenuItem'
import TextField from '@mui/material/TextField'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import DesktopDatePicker from '@mui/lab/DesktopDatePicker'
import LocalizationProvider from '@mui/lab/LocalizationProvider'

// Import custom components
import Layout from '../../../../components/Layout'
import RequestPlayer from '../../../../components/RequestPlayer'

// Import custom functions
import formatAPIErrorMessage from '../../../../utils/formatAPIErrorMessage'

// Import config
import { API_ENDPOINT } from '../../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../../config/api/hostApi'

interface IIngredient {
  id: string
  title: string
  description: string
  quantity: number
  unit: string
}

interface ISubmittedIngredient extends Partial<IIngredient> {
  id?: string
}

interface IIngredientEditComponent {
  ingredient: IIngredient
  handleClickEditIcon: () => void
}

interface IProps {
  ingredients: IIngredient[]
}

// GraphQL query
const queryReadEveryIngredients = gql`
  query Query {
  readUserSelf {
    ingredients {
      id
      title
      description
      quantity
      unit
    }
  }
}`

// GraphQL query
const queryDeleteItem = gql`
mutation DeleteItemById($deleteIngredientId: String!) {
  deleteIngredient(id: $deleteIngredientId) {
    id
  }
}`

// GraphQL query
const queryCreateIngredient = gql`
  mutation Mutation($title: String!, $description: String!, $quantity: Float, $unit: String) {
  createIngredient(title: $title, description: $description, quantity: $quantity, unit: $unit) {
    id
  }
}`

const queryUpdateIngredientByID = gql`
  mutation Mutation($id: String!, $title: String, $description: String, $quantity: Float, $unit: String) {
  updateIngredient(id: $id, title: $title, description: $description, quantity: $quantity, unit: $unit) {
    id
  }
}`

// Editable Ingredient Item
const IngredientWidget = ({
  ingredient,
  handleClickEditIcon,
}: IIngredientEditComponent) => (
  <div
    className='border-4 border-blue-unni rounded-lg 
      flex flex-col justify-between items-center space-y-2 bg-gray-unni
      computer:w-44 computer:h-52 
      tablet:w-44 tablet:h-52'
  >
    <span className='text-base font-montserrat'>{ingredient.title}</span>
    <div
      className='rounded-full h-12 w-12 bg-white border-blue-unni border-2 flex flex-col items-center justify-center
        tablet:border-4 tablet:h-20 tablet:w-20
        computer:border-4 computer:h-20 computer:w-20'
    >
      {/* <Image src={ingredient.src} width={30} height={30}></Image> */}
    </div>
    <div className='space-x-2 text-xs'>
      {/* <span>{ingredient.price}€</span> */}
      <span>.</span>
      <span>{ingredient.quantity} produit(s)</span>
    </div>
    <button
      onClick={handleClickEditIcon}
      className='h-12 w-full bg-blue-secondary space-x-2 text-sm flex items-center justify-center'
    >
      <span>
        <Image src={'/edit.png'} width={20} height={20}></Image>
      </span>
      <span className='text-white'>Modifier</span>
    </button>
  </div>
)

// item for edit dotted component
const NewIngredientWidget = () => (
  <button
    className='border-4 border-dashed border-blue-unni rounded-lg
        flex flex-col justify-center items-center bg-gray-unni
        computer:w-44 computer:h-52 
        tablet:w-44 tablet:h-52'
  >
    <div className='h-20 w-20 flex flex-col items-center justify-center'>
      <Image src={'/plus.png'} width={40} height={40}></Image>
    </div>
    <p>Ajouter</p>
  </button>
)

interface IIngredientForm {
  ingredient?: IIngredient
  onSubmit: (arg0: ISubmittedIngredient) => void
  onDelete: () => void
}

// right side for edit ingredient
const IngredientForm = ({ onSubmit, onDelete, ingredient }: IIngredientForm) => {

  const [ingredientId, setIngredientId] = useState<string | null>('')
  const [ingredientTitle, setIngredientTitle] = useState<string>('')
  const [ingredientDescription, setIngredientDescription] = useState<string>('')
  const [ingredientQuantity, setIngredientQuantity] = useState<string>('0')
  const [ingredientUnit, setIngredientUnit] = useState<string>('GRAM')

  useEffect(() => {

    if (ingredient && ingredient.id !== ingredientId) {
      if (ingredient.id) setIngredientId(ingredient.id)
      if (ingredient.title) setIngredientTitle(ingredient.title)
      if (ingredient.description) setIngredientDescription(ingredient.description)
      if (ingredient.quantity) setIngredientQuantity(ingredient.quantity.toString())
      if (ingredient.unit) setIngredientUnit(ingredient.unit)
    } else {
      setIngredientId(null)
      setIngredientTitle('')
      setIngredientDescription('')
      setIngredientQuantity('0')
      setIngredientUnit('GRAM')
    }

  }, [ingredient])

  // Change Handlers
  const handleChangeTitle = (event: ChangeEvent<HTMLInputElement>) => setIngredientTitle(event.target.value)
  const handleChangeDescription = (event: ChangeEvent<HTMLInputElement>) => setIngredientDescription(event.target.value)
  const handleChangeQuantity = (event: ChangeEvent<HTMLInputElement>) => setIngredientQuantity(event.target.value)
  const handleChangeUnit = (event: ChangeEvent<HTMLInputElement>) => setIngredientUnit(event.target.value)

  const [value, setValue] = useState<Date | null>(new Date())

  const handleChangeDate = (newValue: Date | null) => {
    setValue(newValue)
  }

  const onSubmitHandler = (event: FormEvent) => {
    event.preventDefault()

    const submittedIngredient: ISubmittedIngredient = {
      id: ingredientId || undefined,
      title: ingredientTitle,
      description: ingredientDescription,
      quantity: parseFloat(ingredientQuantity),
      unit: ingredientUnit
    }

    onSubmit(submittedIngredient)
  }

  return (
    <div
      className='rounded-lg flex flex-col justify-between items-center bg-gray-unni
        computer:w-full computer:h-full
        tablet:w-full tablet:h-full'
    >
      <h1 className='font-montserrat'>Modification de l'ingrédient</h1>
      <Box
        className='grid grid-cols-3 grid-row-3 w-full p-2 content-center gap-x-4 gap-y-8'
        component='form'
        sx={{
          '& > :not(style)': {
            width: 'auto',
            height: 'auto',
            fontFamily: 'MontserratAlternates',
          },
        }}
        noValidate
        autoComplete='off'
      >

        <TextField
          className='col-span-1 w-32'
          id='outlined-basic'
          variant='outlined'
          label='Nom du produit'
          size='medium'
          value={ingredientTitle}
          onChange={handleChangeTitle}
        />

        <LocalizationProvider dateAdapter={AdapterDateFns} size='medium'>
          <DesktopDatePicker
            label='Date'
            inputFormat='dd/MM/yyyy'
            value={value}
            onChange={handleChangeDate}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>


        <button type='button'
          className='row-span-3 w-1/2 p-2 border-dashed border-2 border-blue-unni rounded-lg'>
          <Image src={'/plus.png'} width={20} height={20} />
          <p>Inserer une image</p>
        </button>

        <TextField
          className='col-span-2 w-32'
          id='outlined-basic'
          variant='outlined'
          label='Description'
          size='medium'
          value={ingredientDescription}
          onChange={handleChangeDescription}
        />

        <TextField
          id='outlined-basic'
          label='Quantité'
          variant='outlined'
          size='medium'
          value={ingredientQuantity}
          onChange={handleChangeQuantity}
        />

        <TextField
          id='outlined-select-currency'
          select
          label='Unit'
          size='medium'
          value={ingredientUnit}
          onChange={handleChangeUnit}
        >
          <MenuItem key='GRAM' value='GRAM'>
            g
          </MenuItem>
          <MenuItem key='LITER' value='LITER'>
            L
          </MenuItem>
          <MenuItem key='SINGULAR' value='SINGULAR'>
            Singular
          </MenuItem>
        </TextField>

        {/* <TextField
          className='col-span-2'
          id='outlined-basic'
          label='Code barre'
          variant='outlined'
          size='medium'
        /> */}

      </Box>

      <div className='flex flex-col w-full justify-cente items-center'>
        <hr className='bg-black h-0.5 rounded-lg mb-3 mt-3 w-4/5' />
        <div className='flex flex-row justify-center w-full space-x-2'>
          {!!ingredientId &&
            <button
              type='button'
              onClick={onDelete}
              className='text-red-unni font-montserrat bg-white border border-red-unni rounded-lg h-12 w-1/3 mt-2 mb-2 hover:bg-red-unni hover:text-white'>
              Supprimer
            </button>
          }
          <button
            type='submit'
            onClick={onSubmitHandler}
            className='text-blue-unni font-montserrat bg-white border border-blue-unni rounded-lg h-12 w-1/3 mt-2 mb-2 hover:bg-blue-unni hover:text-white'>
            Enregistrer
          </button>
        </div>
      </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: any) => {

  const token = context.req.cookies?.jwt || null

  try {

    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', token)
    const data = await client.request(queryReadEveryIngredients)
    return { props: { ingredients: data.readUserSelf.ingredients } }

  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

// Constants: Initial state
const INITIAL_STATE_ERROR_MESSAGE = ''
const INITIAL_STATE_REQUEST_STATUS = 'NOT_STARTED'
const INITIAL_STATE_INGREDIENT_TO_EDIT = null
const INITIAL_STATE_HAS_ADD_WIDGET_BEEN_CLICKED = false

const Page: NextPage<IProps> = ({ ingredients }) => {

  // State
  const [ingredientList, setIngredientList] = useState(ingredients.slice())
  const [errorMessage, setErrorMessage] = useState<string>(INITIAL_STATE_ERROR_MESSAGE)
  const [requestStatus, setRequestStatus] = useState<string>(INITIAL_STATE_REQUEST_STATUS)
  const [ingredientToEdit, setIngredientToEdit] = useState<number | null>(INITIAL_STATE_INGREDIENT_TO_EDIT)
  const [hasAddWidgetBeenClicked, setHasAddWidgetBeenClicked] = useState<boolean>(INITIAL_STATE_HAS_ADD_WIDGET_BEEN_CLICKED)

  const resetDisplay = () => {
    fetchEveryIngredient()

    setErrorMessage(INITIAL_STATE_ERROR_MESSAGE)
    setRequestStatus(INITIAL_STATE_REQUEST_STATUS)
    setIngredientToEdit(INITIAL_STATE_INGREDIENT_TO_EDIT)
    setHasAddWidgetBeenClicked(INITIAL_STATE_HAS_ADD_WIDGET_BEEN_CLICKED)
  }

  const fetchEveryIngredient = async () => {

    const client = new GraphQLClient(HOST_GRAPHQL_ENDPOINT)
    client.setHeader('authorization', Cookies.get('jwt') as string)
    const data = await client.request(queryReadEveryIngredients)
    setIngredientList(data.readUserSelf.ingredients.slice())
  }

  const handleClickOnItem = (index: number) => {
    setIngredientToEdit(ingredientToEdit === index ? null : index)
    setHasAddWidgetBeenClicked(false)
  }

  const handleClickAddWidget = () => {
    setIngredientToEdit(null)
    setHasAddWidgetBeenClicked(current => !current)
  }

  const onSubmitHandler = async (ingredient: ISubmittedIngredient) => {
    try {

      setErrorMessage('')
      setRequestStatus('LOADING')

      const client = new GraphQLClient(
        HOST_GRAPHQL_ENDPOINT,
        { headers: { authorization: Cookies.get('jwt') as string } }
      )

      const query = ingredient.id ? queryUpdateIngredientByID : queryCreateIngredient
      await client.request(query, ingredient)

      setRequestStatus('DONE')

    } catch (error: any) {
      console.error(error)
      setRequestStatus('ERROR')
      setErrorMessage(formatAPIErrorMessage(error.message || 'error'))
    }
  }

  const onDeleteHandler = async (id: string) => {
    try {

      setErrorMessage('')
      setRequestStatus('LOADING')

      const variables = { deleteIngredientId: id }
      const client = new GraphQLClient(
        HOST_GRAPHQL_ENDPOINT,
        { headers: { authorization: Cookies.get('jwt') as string } }
      )

      await client.request(queryDeleteItem, variables)
      setRequestStatus('DONE')

    } catch (error: any) {
      console.error(error)
      setRequestStatus('ERROR')
      setErrorMessage(formatAPIErrorMessage(error.message || 'error'))
    }
  }

  const hasAnIngredientBeenSelected = Number.isInteger(ingredientToEdit)
  const doDisplayIngredientForm = hasAnIngredientBeenSelected || hasAddWidgetBeenClicked

  return <Layout page='Ingredients'>

    <div className='hidden font-montserrat w-full tablet:flex computer:flex tablet:h-full tablet:justify-between computer:justify-between overflow-hidden'>
      {/* list of all ingredients in edit mode */}
      <article className='flex flex-wrap justify-evenly h-full overflow-auto gap-8'>
        <div onClick={handleClickAddWidget}>
          <NewIngredientWidget />
        </div>
        {ingredientList.map((element: IIngredient, index: number) => (
          <IngredientWidget
            key={index}
            ingredient={element}
            handleClickEditIcon={() => handleClickOnItem(index)}
          />
        ))}
      </article>

      {doDisplayIngredientForm && (
        <aside className='rounded-lg w-2/3 h-full'>
          <RequestPlayer
            status={requestStatus}
            callbackSuccess={resetDisplay}
            callbackError={resetDisplay}
          >
            <IngredientForm
              ingredient={ingredientList[ingredientToEdit as number]}
              onSubmit={onSubmitHandler}
              // onDelete={onDeleteHandler}
              onDelete={() => onDeleteHandler(ingredientList[ingredientToEdit as number].id)}
            />
          </RequestPlayer>
        </aside>
      )}

    </div>

  </Layout>
}

export default Page
