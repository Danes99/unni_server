// Import pre-installed modules
import type { NextPage } from 'next'
import { useState, useEffect, useRef } from 'react'

// Import downloaded modules
import * as d3 from 'd3'

// Import custom components
import Layout from '../../../../components/Layout'

// Constants: Initial State
const INITIAL_STATE_DATASET = [100, 200, 300, 400, 500]

const Page: NextPage = () => {

  // State
  const [dataSet,] = useState(INITIAL_STATE_DATASET)

  // Ref
  const myRef = useRef(null)

  useEffect(() => {

    let size = 500;
    let rect_width = 95;

    let svg = d3.select(myRef.current)
      .append('svg')
      .attr('width', size)
      .attr('height', size);

    svg.selectAll('rect')
      .data(dataSet)
      .enter()
      .append('rect')
      .attr('x', (d, i) => 5 + i * (rect_width + 5))
      .attr('y', d => size - d)
      .attr('width', rect_width)
      .attr('height', d => d)
      .attr('fill', 'teal')

  },
    [])

  return (
    <Layout page='Analytics'>
      <div
        ref={myRef}
      >

      </div>
    </Layout>
  )
}

export default Page
