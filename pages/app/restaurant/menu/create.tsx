// Import pre-installed modules
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'
import { useState, FormEvent, useEffect } from 'react'
import { useRouter } from 'next/router'

// Import downloaded modules
import { Message } from 'semantic-ui-react'
import { Player } from '@lottiefiles/react-lottie-player'
import { GraphQLClient, gql } from 'graphql-request'
import Cookies from 'js-cookie'

// Import custom components
import Layout from '../../../../components/Layout'

// Import custom functions
import formatAPIErrorMessage from '../../../../utils/formatAPIErrorMessage'

// Import config
import { API_ENDPOINT } from '../../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../../config/api/hostApi'

const queryCreateRecipe = gql`
  mutation Mutation($title: String!, $description: String!, $price: Float!, $ingredients: [RecipeIngredientInputType]) {
  createRecipe(title: $title, description: $description, price: $price, ingredients: $ingredients) {
    id
  }
}`

const queryReadEveryIngredient = gql`
query Query {
  readUserSelf {
    ingredients {
      id
      title
    }
  }
}`

interface IIngredient {
  id: string
  title: string
  quantity?: number
}

interface IProps {
  ingredients: [IIngredient]
}

interface IRecipeIngredient {
  id: string
  quantity: number
}

interface IFormTarget {
  title?: { value: string }
  description?: { value: string }
  price?: { value: string }
  category?: { value: string }
  recipeIngredients?: {
    value: [IRecipeIngredient]
  }
}

// Constants: initial state
const INITIAL_STATE_HAS_CREATE_REQUEST_ENDED = false
const INITIAL_STATE_HAS_CREATE_REQUEST_STARTED = false
const INITIAL_STATE_CREATE_REQUEST_ERROR = ""
const INITIAL_STATE_SELECTED_MENU_INGREDIENTS: IIngredient[] = []

export const getServerSideProps: GetServerSideProps = async (context: any) => {

  try {
    const token = context.req.cookies?.jwt || null

    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', token)
    const data = await client.request(queryReadEveryIngredient)
    return { props: { ingredients: data.readUserSelf.ingredients } }

  } catch (error) {
    console.error(error)
    return { props: { ingredients: [] } }
  }
}

const Page: NextPage<IProps> = ({ ingredients }) => {

  const router = useRouter()
  // 

  // State
  const [hasCreateRequestStarted, setHasCreateRequestStarted] = useState(INITIAL_STATE_HAS_CREATE_REQUEST_STARTED)
  const [hasCreateRequestEnded, setHasCreateRequestEnded] = useState(INITIAL_STATE_HAS_CREATE_REQUEST_ENDED)
  const [createRequestError, setCreateRequestError] = useState(INITIAL_STATE_CREATE_REQUEST_ERROR)

  // State: Recipe ingredients
  const [proposedRecipeIngredients,] = useState(ingredients)
  const [selectedRecipeIngredients, setSelectedRecipeIngredients] = useState(INITIAL_STATE_SELECTED_MENU_INGREDIENTS.slice())

  const [, setDoRefresh] = useState(true)

  const onClickProposedRecipeIngredientHandler = async (index: number) => {
    selectedRecipeIngredients.push(proposedRecipeIngredients.splice(index, 1)[0])
    setDoRefresh(current => !current)
  }

  const onClickSelectedRecipeIngredientHandler = async (index: number) => {
    proposedRecipeIngredients.push(selectedRecipeIngredients.splice(index, 1)[0])
    setDoRefresh(current => !current)
  }

  const updateQuantitySelectedRecipeIngredientHandler = async (index: number, quantity: number) => {
    selectedRecipeIngredients[index].quantity = quantity
  }

  const onSubmitHandler = async (event: FormEvent) => {
    event.preventDefault()

    setHasCreateRequestStarted(true)

    try {

      const endpoint = HOST_GRAPHQL_ENDPOINT
      const jwt = Cookies.get('jwt') as string

      const target = event.target as IFormTarget

      const createRecipeVariables = {
        title: target.title?.value,
        description: target.description?.value,
        price: parseFloat(target.price?.value || '0'),
        category: target.category?.value,
        ingredients: selectedRecipeIngredients.map(
          element => ({
            ingredientId: element.id,
            quantity: element.quantity
          })
        )
      }

      const client = new GraphQLClient(
        endpoint,
        { headers: { authorization: jwt } }
      )

      const data = await client.request(queryCreateRecipe, createRecipeVariables)

    } catch (error) {
      console.error(error)
      setCreateRequestError(error instanceof Error ? error.message : 'error')
    }

    setSelectedRecipeIngredients(current => INITIAL_STATE_SELECTED_MENU_INGREDIENTS.slice())
    setHasCreateRequestEnded(true)
  }

  const createIngredientForm =
    <div className="max-w-md w-full space-y-8 p-10 bg-white rounded-xl shadow-lg z-10">
      <div className="grid gap-8 grid-cols-1">
        <div className="flex flex-col">

          <div className="flex flex-col sm:flex-row items-center">
            <h2 className="font-semibold text-lg mr-auto">Recipe Info</h2>
            <div className="w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0"></div>
          </div>

          <form className="mt-5" onSubmit={onSubmitHandler}>

            <div className="md:flex flex-row md:space-x-4 w-full text-xs">
              <div className="mb-3 space-y-2 w-full text-xs">
                <label className="font-semibold text-gray-600 py-2">Name<abbr title="required">*</abbr></label>
                <input
                  className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4"
                  placeholder="Name" required type="text" name="title" id="title" />
              </div>
            </div>

            <div className="flex-auto w-full mb-1 text-xs space-y-2">
              <label className="font-semibold text-gray-600 py-2">Description</label>
              <textarea
                className="w-full h-12 appearance-none block bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg py-4 px-4"
                required name="description" id="description" placeholder="Enter the ingredient description">
              </textarea>
            </div>

            <div className="md:flex md:flex-row md:space-x-4 w-full text-xs">

              <div className="w-full flex flex-col mb-3">
                <label className="font-semibold text-gray-600 py-2">Price</label>
                <input
                  className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4"
                  required placeholder="Price" type="number" name="price" id="price" />
              </div>

              <div className="w-full flex flex-col mb-3">
                <label className="font-semibold text-gray-600 py-2">Category<abbr title="required">*</abbr></label>
                <select
                  className="block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4 md:w-full"
                  required name="category" id="category">
                  <option value="DISH">Dish</option>
                  <option value="DRINK">Drink</option>
                  <option value="DISH_SINGULAR">Dish (Singular)</option>
                  <option value="DRINK_SINGULAR">Drink (Singular)</option>
                </select>
              </div>

              <div className="w-full flex flex-col mb-3">
                {/* <label className="font-semibold text-gray-600 py-2">Unit<abbr title="required">*</abbr></label>
                <select
                  className="block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4 md:w-full"
                  required name="unit" id="unit">
                  <option value="GRAM">g (Gram)</option>
                  <option value="LITER">L (Liter)</option>
                </select> */}
              </div>

            </div>

            <div className="md:flex md:flex-row md:space-x-4 w-full text-xs">

              <div className="w-full flex flex-col mb-3">
                <label className="font-semibold text-gray-600 py-2">Ingredients<abbr title="required">*</abbr></label>
                <select
                  className="block w-full h-28 bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg px-4 md:w-full"
                  multiple name="recipeIngredients" id="recipeIngredients">
                  {proposedRecipeIngredients.map(
                    (element: IIngredient, index: number) =>
                      <option key={element.id} value={element.id}
                        onClick={() => onClickProposedRecipeIngredientHandler(index)}
                      >
                        {element.title}
                      </option>
                  )}
                </select>
              </div>

              {/* <div className="w-full flex flex-col mb-3">
                <label className="font-semibold text-gray-600 py-2">Ingredients<abbr title="required">*</abbr></label>
                <div
                  className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4">
                  {selectedRecipeIngredients.map(
                    (element: IIngredient, index: number) =>
                      <div
                        key={element.id}
                        className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 py-10 px-4 text-center"
                      >
                        {element.title}
                      </div>
                  )}
                </div>
              </div> */}

            </div>

            <div className="w-full flex flex-col mb-3">
              <label className="font-semibold text-gray-600 py-2">Ingredients</label>
              {selectedRecipeIngredients.map(
                (element: IIngredient, index: number) =>
                  <div
                    key={element.id}
                    className="flex items-center justify-center appearance-none w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 py-5 px-4 text-center"
                  >
                    <div className='flex-grow'
                      onClick={(event) => onClickSelectedRecipeIngredientHandler(index)}
                    >
                      {element.title}
                    </div>
                    <input type="number"
                      id={`ingredient_quantity_${index}`}
                      name={element.id}
                      placeholder="0"
                      onChange={(event) => updateQuantitySelectedRecipeIngredientHandler(index, parseFloat(event.target.value))}
                      className="appearance-none text-grey-darker w-20 px-4 text-center"
                    />
                  </div>
              )}
            </div>

            <div className="mb-3 space-y-2 w-full text-xs">
              <label className=" font-semibold text-gray-600 py-2">Ingredient Image (Optional)</label>
              <div className="flex flex-wrap items-stretch w-full mb-4 relative">
                <div className="flex">
                  <span className="flex leading-normal bg-grey-lighter border-1 rounded-r-none border border-r-0 border-blue-300 px-3 whitespace-no-wrap text-grey-dark w-12 h-10 bg-blue-300 justify-center items-center  text-xl rounded-lg text-white">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                    </svg>
                  </span>
                </div>
                <input type="text" className="flex-shrink flex-grow leading-normal w-px flex-1 border border-l-0 h-10 border-grey-light rounded-lg rounded-l-none px-3 relative focus:border-blue focus:shadow" placeholder="https://" />
              </div>
            </div>

            <p className="text-xs text-red-500 text-right my-3">
              Required fields are marked with an asterisk <abbr title="Required field">*</abbr>
            </p>

            <div className="mt-5 text-right md:space-x-3 md:block flex flex-col-reverse">
              <button type="reset" className="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100 text-center" onClick={router.back}>Cancel</button>
              <button type="submit" className="mb-2 md:mb-0 bg-green-400 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-green-500">Save</button>
            </div>
          </form>

        </div>

      </div>
    </div>

  return (
    <Layout page='New Ingredient'>

      <div className="flex flex-col items-center">

        {hasCreateRequestStarted ?
          hasCreateRequestEnded ?
            createRequestError ?

              <div className="w-96">
                <Player src="/lottiefiles/lottiefiles_error_lf30_editor.json" background="transparent" speed={1} autoplay loop />
                <div className="mt-20">
                  <Message negative>
                    <Message.Header>Error</Message.Header>
                    <p>{formatAPIErrorMessage(createRequestError)}</p>
                  </Message>
                </div>
              </div>
              :
              <Player className="w-96" src="/lottiefiles/lottiefiles_validated_lf30_editor.json" background="transparent" speed={1} autoplay
                onEvent={event => { if (event === 'complete') router.back() }}
              />
            :
            <Player className="w-96" src="/lottiefiles/lottiefiles_loader_several_points_lf20.json" background="transparent" speed={1} loop autoplay />
          :
          createIngredientForm}

      </div>


    </Layout>
  )
}

// Tailwindcss Form link
// https://tailwindcomponents.com/component/forms

export default Page
