// Import framework modules
import Image from 'next/image'
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'
import { useState, useEffect, ChangeEvent, FormEvent } from 'react'

// Import downloaded modules
import Cookies from 'js-cookie'
import { GraphQLClient, gql } from 'graphql-request'

// Import downloaded modules: MUI
import Box from '@mui/material/Box'
import MenuItem from '@mui/material/MenuItem'
import TextField from '@mui/material/TextField'

// Import custom components
import Layout from '../../../../components/Layout'
import RequestPlayer from '../../../../components/RequestPlayer'

// Import config
import { API_ENDPOINT } from '../../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../../config/api/hostApi'

interface IRecipeIngredient {
  id: string
  ingredientId: string
  quantity: number
  // details: {
  //   title: string
  // }
}

interface IIngredient {
  id: string
  title: string
  quantity?: number
}

interface IRecipe {
  id: string
  createdAt: string
  updatedAt: string
  title: string
  description: string
  price: number
  category: string
  ingredients: [IRecipeIngredient]
}

// GraphQL query
const queryReadEveryRecipes = gql`
  query Query {
  readUserSelf {
    recipes {
      id
      createdAt
      updatedAt
      title
      description
      price
      category
      ingredients {
        id
        ingredientId
        quantity
        # details {
        #   title
        # }
      }
    }
    ingredients {
      id
      title
    }
  }
}`

const queryReadEveryIngredients = gql`
  query Query {
  readUserSelf {
    ingredients {
      id
      title
      description
      quantity
      unit
    }
  }
}`

const queryCreateRecipe = gql`
  mutation Mutation($title: String!, $description: String!, $price: Float!, $ingredients: [RecipeIngredientInputType]) {
  createRecipe(title: $title, description: $description, price: $price, ingredients: $ingredients) {
    id
  }
}`

const queryDeleteItem = gql`
mutation DeleteItemById($id: String!) {
  deleteRecipe(id: $id) {
    id
  }
}`

interface IWidgetProposedIngredient {
  item: ISubmitRecipeIngredient
}

// item for phone/tablet component
const WidgetProposedIngredient = ({ item }: IWidgetProposedIngredient) => (
  <div
    className='border-2 border-blue-unni rounded-lg p-2 flex flex-col 
      justify-between items-center w-36 h-36 bg-gray-unni
      computer:w-44 computer:h-44 computer:border-4 computer:space-y-2
      tablet:w-44 tablet-44 tablet:border-4 tablet:space-y-2'
  >
    <div
      className='text-xs
        tablet:text-base
        computer:text-base'
    >
      {item.title}
    </div>
    <div
      className='rounded-full h-12 w-12 bg-white border-blue-unni 
        border-2 flex flex-col items-center justify-center
        tablet:border-4 tablet:h-20 tablet:w-20
        computer:border-4 computer:h-20 computer:w-20'
    >
      <Image src={'/legume.png'} width={25} height={25}></Image>
    </div>
    <div className='space-x-2 text-xs'>
      {/* <span>{item.price}€</span>
      <span>.</span>
      <span>{item.quantity} produit(s)</span> */}
    </div>
  </div>
)

interface IPropsWidgetRecipe {
  recipe: IRecipe
  onEdit: () => void
}

// item for edit component
const WidgetRecipe = ({ recipe, onEdit }: IPropsWidgetRecipe) => (
  <div
    className='border-4 border-blue-unni rounded-lg flex flex-col 
      justify-between items-center space-y-2 bg-gray-unni
      computer:w-44 computer:h-52 
      tablet:w-44 tablet:h-52'
  >
    <span className='text-base font-montserrat'>{recipe.title}</span>
    <div
      className='rounded-full h-12 w-12 bg-white border-blue-unni 
        border-2 flex flex-col items-center justify-center
        tablet:border-4 tablet:h-20 tablet:w-20
        computer:border-4 computer:h-20 computer:w-20'
    >
      <Image src={'/cart.png'} width={30} height={30}></Image>
    </div>
    <div className='space-x-2 text-xs'>
      <span>{recipe.price}€</span>
      {/* <span>.</span>
      <span>45 produit(s)</span> */}
    </div>
    <button
      type='button'
      onClick={onEdit}
      className='h-12 w-full bg-blue-secondary space-x-2 text-sm flex items-center justify-center'>
      <span>
        <Image src={'/edit.png'} width={20} height={20}></Image>
      </span>
      <span className='text-white'>Modifier</span>
    </button>
  </div>
)

const NewRecipeWidget = () => (
  <button
    className='border-4 border-dashed border-blue-unni rounded-lg
        flex flex-col justify-center items-center bg-gray-unni
        computer:w-44 computer:h-52 
        tablet:w-44 tablet:h-52'
  >
    <div className='h-20 w-20 flex flex-col items-center justify-center'>
      <Image src={'/plus.png'} width={40} height={40}></Image>
    </div>
    <p>Ajouter</p>
  </button>
)

const filterProposedIngredients = (proposedIngredients: IIngredient[], recipeIngredients: IRecipeIngredient[]): ISubmitRecipeIngredient[] => {

  const filtered = proposedIngredients.filter((value: IIngredient) => {
    const index = recipeIngredients.findIndex(
      (element: IRecipeIngredient) => element.ingredientId === value.id
    )
    return index < 0
  })

  return filtered.map((element: IIngredient): ISubmitRecipeIngredient => ({
    ...element,
    quantity: 0
  }))
}

const getIngredientNameFromId = (id: string, ingredients: IIngredient[]) => (
  ingredients.find((element: IIngredient) => element.id === id)?.title || ''
)

const convertRecipeIngredientToIngredient = (recipeIngredients: IRecipeIngredient[], ingredients: IIngredient[]): ISubmitRecipeIngredient[] => (
  recipeIngredients.map((element: IRecipeIngredient) => ({
    id: element.ingredientId,
    title: getIngredientNameFromId(element.ingredientId, ingredients),
    quantity: element.quantity
  }))
)

interface ISubmitRecipeIngredient {
  id: string
  title?: string
  quantity: number
}

interface ISubmittedRecipe {
  id?: string
  title: string
  description: string
  price: number
  category: string
  ingredients?: [ISubmitRecipeIngredient]
}

interface IPropsAsideForm {
  ingredients: IIngredient[]
  recipe?: IRecipe
  onSubmit: (arg0: ISubmittedRecipe) => void
  onDelete: () => void
}

const AsideForm = ({ ingredients, recipe, onDelete, onSubmit }: IPropsAsideForm) => {

  const [, setDoRefresh] = useState<boolean>(false)

  const [itemId, setItemId] = useState('')
  const [itemTitle, setItemTitle] = useState<string>('')
  const [itemDescription, setItemDescription] = useState<string>('')
  const [itemPrice, setItemPrice] = useState<string>('0')
  const [itemCategory, setItemCategory] = useState<string>('DISH')

  const [selectedIngredients, setSelectedIngredients] = useState<ISubmitRecipeIngredient[]>(
    convertRecipeIngredientToIngredient(recipe?.ingredients || [], ingredients)
  )

  const proposedIngredients: ISubmitRecipeIngredient[] = filterProposedIngredients(ingredients, recipe?.ingredients || [])

  useEffect(() => {

    if (recipe && recipe.id !== itemId) {
      if (recipe.id) setItemId(recipe.id)
      if (recipe.title) setItemTitle(recipe.title)
      if (recipe.description) setItemDescription(recipe.description)
      if (recipe.price) setItemPrice(recipe.price.toString())
      if (recipe.category) setItemCategory(recipe.category)
      if (recipe.ingredients) setSelectedIngredients(
        convertRecipeIngredientToIngredient(recipe.ingredients, ingredients)
      )
    } else {
      setItemId('')
      setItemTitle('')
      setItemDescription('')
      setItemPrice('0')
      setItemCategory('DISH')
      setSelectedIngredients([])
    }

  }, [recipe])

  // Change Handlers
  const handleChangeTitle = (event: ChangeEvent<HTMLInputElement>) => setItemTitle(event.target.value)
  const handleChangeDescription = (event: ChangeEvent<HTMLInputElement>) => setItemDescription(event.target.value)
  const handleChangePrice = (event: ChangeEvent<HTMLInputElement>) => setItemPrice(event.target.value)
  const handleChangeCategory = (event: ChangeEvent<HTMLInputElement>) => setItemCategory(event.target.value)

  const onClickProposedIngredientHandler = async (id: String) => {

    // selectedIngredients.push(selectedIngredients.splice(index, 1)[0])
    setDoRefresh(current => !current)
  }

  const onClickSelectedIngredientHandler = async (index: number) => {
    // selectedIngredients.splice(index, 1)
    setDoRefresh(current => !current)
  }

  const updateQuantitySelectedRecipeIngredientHandler = async (index: number, quantity: number) => {
    // selectedRecipeIngredients[index].quantity = quantity
  }

  const onSubmitHandler = (event: FormEvent) => {
    event.preventDefault()
    const submittedRecipe: ISubmittedRecipe = {
      id: itemId,
      title: itemTitle,
      description: itemDescription,
      price: parseFloat(itemPrice),
      category: itemCategory
    }
    onSubmit(submittedRecipe)
  }

  return <>

    <div className='rounded-lg w-96 h-full'>
      <div
        className='rounded-lg flex flex-col justify-between items-center bg-gray-unni
      computer:w-96 computer:h-full 
      tablet:w-96 tablet:h-full'
      >
        <h1>Ingrédients</h1>
        <div className='h-10 w-72 bg-white rounded-lg flex flex-row justify-around mb-2'>
          <input placeholder='Rechercher'></input>
          <div className='h-10 w-10 flex flex-col items-center justify-center'>
            <Image src={'/search.png'} width={20} height={20}></Image>
          </div>
        </div>
        <div className='flex flex-wrap  justify-center gap-y-1 gap-x-2 overflow-auto h-full mt-2'>
          {proposedIngredients.map((element: ISubmitRecipeIngredient, index: number) =>
            <WidgetProposedIngredient key={index} item={element} />
          )}
        </div>
        <button className='text-white font-montserrat bg-blue-unni rounded-lg h-12 w-72 mb-2 mt-2'>
          Ajouter
        </button>
      </div>
    </div>

    <div className='rounded-lg w-96 h-full'>
      <div
        className='rounded-lg flex flex-col justify-between 
        items-center bg-gray-unni
        computer:w-full computer:h-full
        tablet:w-full tablet:h-full'
      >
        <h1>Création du produit</h1>
        <hr className='bg-blue-unni h-0.5 rounded-lg m-3 w-4/5' />
        <Box
          className='grid grid-cols-3 grid-row-3 w-full p-2 content-center gap-x-4 gap-y-4'
          component='form'
          sx={{
            '& > :not(style)': {
              width: 'auto',
              height: 'auto',
              fontFamily: 'MontserratAlternates',
            },
          }}
          noValidate
          autoComplete='off'
        >
          <TextField
            className='col-span-2 w-32'
            id='outlined-basic'
            label='Nom du produit'
            variant='outlined'
            size='medium'
            value={itemTitle}
            onChange={handleChangeTitle}
          />
          <button className='row-span-3 w-1/2 p-2 border-dashed border-2 border-blue-unni rounded-lg'>
            <Image src={'/plus.png'} width={20} height={20}></Image>
            <p>Inserer une image</p>
          </button>
          <TextField
            className='col-span-2 w-32'
            id='outlined-basic'
            label='Description'
            variant='outlined'
            size='medium'
            value={itemDescription}
            onChange={handleChangeDescription}
          />
          <TextField
            id='outlined-basic-currency'
            label='Prix'
            size='small'
            value={itemPrice}
            onChange={handleChangePrice}
          />
          <TextField
            select
            id='outlined-select'
            label='Catégorie'
            size='small'
            value={itemCategory}
            onChange={handleChangeCategory}
          >
            <MenuItem key='DISH' value='DISH'>
              Plat
            </MenuItem>
            <MenuItem key='DISH_SINGULAR' value='DISH_SINGULAR'>
              Plat Unitaire
            </MenuItem>
            <MenuItem key='DRINK' value='DRINK'>
              Boisson
            </MenuItem>
            <MenuItem key='DRINK_SINGULAR' value='DRINK_SINGULAR'>
              Boisson Unitaire
            </MenuItem>
          </TextField>
        </Box>

        <hr className='bg-blue-unni h-0.5 rounded-lg mb-3 mt-3 w-4/5' />
        <div className='w-full p-2'>
          <h4>Ingrédients</h4>
        </div>
        <div className='w-full overflow-auto'>
          {selectedIngredients.map((element: ISubmitRecipeIngredient) =>
            <div className='flex flex-row h-12 items-center justify-around'
              key={element.id}>
              <div className='rounded-full border-2 border-blue-unni 
                bg-white h-8 w-8 flex flex-row justify-center items-center'>
                <Image src={'/beer.png'} width={20} height={20}></Image>
              </div>
              <span>{getIngredientNameFromId(element.id, ingredients)}</span>
              <div className='rounded-lg h-8 w-8 border-blue-unni 
              bg-white border-2 flex flex-row items-center justify-center'>
                {element.quantity}
              </div>
              <div className='rounded-lg h-8 w-8 border-red-600 
                bg-white border-2 flex flex-row items-center justify-center'>
                <Image src={'/remove.png'} width={20} height={20}></Image>
              </div>
            </div>
          )}
        </div>

        <div className='flex flex-col w-full justify-cente items-center'>
          <hr className='bg-black h-0.5 rounded-lg mb-3 mt-3 w-4/5' />
          <div className='flex flex-row justify-center w-full space-x-2'>
            {!!itemId &&
              <button
                type='button'
                onClick={onDelete}
                className='text-red-unni font-montserrat bg-white border border-red-unni rounded-lg h-12 w-1/3 mt-2 mb-2 hover:bg-red-unni hover:text-white'>
                Supprimer
              </button>
            }
            <button
              type='submit'
              onClick={onSubmitHandler}
              className='text-blue-unni font-montserrat bg-white border border-blue-unni rounded-lg h-12 w-1/3 mt-2 mb-2 hover:bg-blue-unni hover:text-white'>
              Enregistrer
            </button>
          </div>
        </div>

      </div>
    </div>
  </>
}

export const getServerSideProps: GetServerSideProps = async (context: any) => {
  try {
    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', context.req.cookies?.jwt)
    const data = await client.request(queryReadEveryRecipes)
    return {
      props: {
        recipes: data.readUserSelf.recipes,
        ingredients: data.readUserSelf.ingredients
      }
    }

  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

// Constants: Initial state
const INITIAL_STATE_ERROR_MESSAGE = ''
const INITIAL_STATE_REQUEST_STATUS = 'NOT_STARTED'
const INITIAL_STATE_ITEM_TO_EDIT = null
const INITIAL_STATE_HAS_ADD_WIDGET_BEEN_CLICKED = false

interface IProps {
  recipes: [IRecipe]
  ingredients: [IIngredient]
}

const Page: NextPage<IProps> = ({ recipes, ingredients }) => {

  // State
  const [recipeList, setRecipeList] = useState(recipes.slice())
  const [errorMessage, setErrorMessage] = useState<string>(INITIAL_STATE_ERROR_MESSAGE)
  const [requestStatus, setRequestStatus] = useState<string>(INITIAL_STATE_REQUEST_STATUS)
  const [itemToEdit, setItemToEdit] = useState<number | null>(INITIAL_STATE_ITEM_TO_EDIT)
  const [hasAddWidgetBeenClicked, setHasAddWidgetBeenClicked] = useState<boolean>(INITIAL_STATE_HAS_ADD_WIDGET_BEEN_CLICKED)

  const [IsClicked, setIsClicked] = useState(true)
  const [doDisplayIngredientList, setDoDisplayIngredientList] = useState(false)

  const resetDisplay = () => {
    fetchEveryRecipe()

    setErrorMessage(INITIAL_STATE_ERROR_MESSAGE)
    setRequestStatus(INITIAL_STATE_REQUEST_STATUS)
    setItemToEdit(INITIAL_STATE_ITEM_TO_EDIT)
    setHasAddWidgetBeenClicked(INITIAL_STATE_HAS_ADD_WIDGET_BEEN_CLICKED)
  }

  const fetchEveryRecipe = async () => {

    const client = new GraphQLClient(HOST_GRAPHQL_ENDPOINT)
    client.setHeader('authorization', Cookies.get('jwt') as string)
    const data = await client.request(queryReadEveryRecipes)
    setRecipeList(data.readUserSelf.recipes.slice())
  }

  const handleClickAddWidget = () => {
    setItemToEdit(null)
    setHasAddWidgetBeenClicked(current => !current)
  }

  const handleClickOnItem = (index: number) => {
    setItemToEdit(current => current === index ? null : index)
    setHasAddWidgetBeenClicked(false)
  }

  const onSubmitHandler = async (recipe: ISubmittedRecipe) => {
    try {

      setErrorMessage('')
      setRequestStatus('LOADING')

      console.log(recipe)

      // const client = new GraphQLClient(
      //   HOST_GRAPHQL_ENDPOINT,
      //   { headers: { authorization: Cookies.get('jwt') as string } }
      // )

      // const query = ingredient.id ? queryUpdateIngredientByID : queryCreateIngredient
      // await client.request(query, ingredient)

      setRequestStatus('DONE')

    } catch (error: any) {
      console.error(error)
      setRequestStatus('ERROR')
      setErrorMessage(error.message || 'error')
    }
  }

  const onDeleteHandler = async (id: string) => {
    try {
      setErrorMessage('')
      setRequestStatus('LOADING')
      const client = new GraphQLClient(
        HOST_GRAPHQL_ENDPOINT,
        { headers: { authorization: Cookies.get('jwt') as string } }
      )
      await client.request(queryDeleteItem, { id })
      setRequestStatus('DONE')

    } catch (error: any) {
      console.error(error)
      setRequestStatus('ERROR')
      setErrorMessage(error.message || 'error')
    }
  }

  const doDisplayForm = Number.isInteger(itemToEdit) || hasAddWidgetBeenClicked

  return (
    <Layout page='Menu'>
      <div className='font-montserrat w-full flex flex-col tablet:flex-row tablet:gap-x-3 computer:flex-row computer:gap-x-3 tablet:h-full justify-between overflow-hidden'>
        {/* Left-side of layout - Categories and items */}

        <article className='flex flex-wrap justify-evenly space-x-2 tablet:h-full tablet:w-2/3 computer:w-3/4 tablet:overflow-auto computer:overflow-auto'>
          <div onClick={handleClickAddWidget}>
            <NewRecipeWidget />
          </div>
          {recipeList.map((element: IRecipe, index: number) =>
            <WidgetRecipe
              key={index}
              onEdit={() => { handleClickOnItem(index) }}
              recipe={element}
            />
          )}
        </article>

        {/* Right-side of layout - Ticket and Payments info */}
        {doDisplayForm &&
          <aside className='hidden computer:flex computer:flex-row tablet:flex tablet:flex-row'>

            <RequestPlayer
              status={requestStatus}
              callbackSuccess={resetDisplay}
              callbackError={resetDisplay}
            >
              <AsideForm
                ingredients={ingredients}
                recipe={recipeList[itemToEdit as number]}
                onSubmit={onSubmitHandler}
                onDelete={() => onDeleteHandler(recipeList[itemToEdit as number].id)}
              />
            </RequestPlayer>
          </aside>}
      </div>
    </Layout >
  )
}

export default Page
