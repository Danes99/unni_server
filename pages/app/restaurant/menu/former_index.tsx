// Import pre-installed modules
import { useState } from 'react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { GetServerSideProps } from 'next'

// Import downloaded modules
import Cookies from 'js-cookie'
import { GraphQLClient, gql } from 'graphql-request'
import { Player } from '@lottiefiles/react-lottie-player'
import { Button, Icon, Table, Popup, Modal, Header } from 'semantic-ui-react'

// Import custom components
import Layout from '../../../../components/Layout'

// Import custom functions
import formatAPIErrorMessage from '../../../../utils/formatAPIErrorMessage'

// Import config
import { API_ENDPOINT } from '../../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../../config/api/hostApi'
import {
  ROUTE_RESTAURANT_MENU_CREATE,
  ROUTE_RESTAURANT_MENU_UPDATE
} from '../../../../config/routes'

interface IRecipe {
  id: string
  title: string
  price: number
}

interface IProps {
  recipes: [IRecipe]
}

// GraphQL query
const queryReadEveryRecipes = gql`
  query Query {
  readUserSelf {
    recipes {
      id
      title
      price
    }
  }
}`

// GraphQL query
const queryDeleteItem = gql`
mutation DeleteItemById($deleteRecipeId: String!) {
  deleteRecipe(id: $deleteRecipeId) {
    id
  }
}`

// Initial state: GraphQL DELETE Request (delete item)
const INITIAL_STATE_ITEM_TO_DELETE = null
const INITIAL_STATE_HAS_DELETE_REQUEST_STARTED = false
const INITIAL_STATE_HAS_DELETE_REQUEST_ENDED = false
const INITIAL_STATE_DELETE_REQUEST_ERROR = ""

export const getServerSideProps: GetServerSideProps = async (context: any) => {

  try {

    const token = context.req.cookies?.jwt || null

    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', token)
    const data = await client.request(queryReadEveryRecipes)
    return { props: { recipes: data.readUserSelf.recipes } }

  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

const Page: NextPage<IProps> = ({ recipes }) => {

  const router = useRouter()

  // State
  const [itemToDelete, setItemToDelete] = useState<number | null>(INITIAL_STATE_ITEM_TO_DELETE)
  const [hasDeleteRequestStarted, setHasDeleteRequestStarted] = useState(INITIAL_STATE_HAS_DELETE_REQUEST_STARTED)
  const [hasDeleteRequestEnded, setHasDeleteRequestEnded] = useState(INITIAL_STATE_HAS_DELETE_REQUEST_ENDED)
  const [deleteRequestError, setDeleteRequestError] = useState(INITIAL_STATE_DELETE_REQUEST_ERROR)

  const resetDeleteModal = () => {
    setItemToDelete(INITIAL_STATE_ITEM_TO_DELETE)
    setDeleteRequestError(INITIAL_STATE_DELETE_REQUEST_ERROR)
    setHasDeleteRequestEnded(INITIAL_STATE_HAS_DELETE_REQUEST_ENDED)
    setHasDeleteRequestStarted(INITIAL_STATE_HAS_DELETE_REQUEST_STARTED)
  }

  const doDisplayModal = itemToDelete !== INITIAL_STATE_ITEM_TO_DELETE

  const deleteModal = doDisplayModal ?
    <Modal
      closeIcon
      open={doDisplayModal}
      dimmer='blurring'
      onClose={resetDeleteModal}
    >
      <Header icon='trash' content={`Delete ${recipes[itemToDelete].title}`} />
      <Modal.Content>

        {hasDeleteRequestStarted ?
          hasDeleteRequestEnded ?
            deleteRequestError ?
              <div className="w-96">
                <Player src="/lottiefiles/lottiefiles_error_lf30_editor.json" background="transparent" speed={1} autoplay loop />
                <div className="mt-20">
                  <p>{deleteRequestError}</p>
                </div>
              </div>
              :
              <Player className="w-96" src="/lottiefiles/lottiefiles_validated_lf30_editor.json" background="transparent" speed={1} autoplay
                onEvent={event => { if (event === 'complete') resetDeleteModal(); window.location.reload() }}
              />
            :
            <Player className="w-96" src="/lottiefiles/lottiefiles_loader_several_points_lf20.json" background="transparent" speed={1} loop autoplay />
          :
          <p>
            Are you sure you want to delete {recipes[itemToDelete].title}?
            The recipe will be permanently removed. This action cannot be undone.
          </p>
        }

      </Modal.Content>
      <Modal.Actions>
        <Button color='red' onClick={resetDeleteModal}>
          <Icon name='remove' /> No
        </Button>
        <Button color='green' onClick={() => onDeleteHandler(recipes[itemToDelete].id)}>
          <Icon name='checkmark' /> Yes
        </Button>
      </Modal.Actions>
    </Modal>
    : null

  const onDeleteHandler = async (id: string) => {

    setHasDeleteRequestStarted(true)

    try {

      // const endpoint = `${API_PROTOCOL}//${API_BASE_URL}:${API_PORT}/${GRAPHQL_ENDPOINT}`
      const endpoint = HOST_GRAPHQL_ENDPOINT
      const variables = { deleteRecipeId: id }
      const jwt = Cookies.get('jwt') as string

      const client = new GraphQLClient(
        endpoint,
        { headers: { authorization: jwt } }
      )
      const data = await client.request(queryDeleteItem, variables)

    } catch (error) {
      console.error(error)
      setDeleteRequestError(
        formatAPIErrorMessage(
          error instanceof Error ? error.message : 'error'
        )
      )
    }

    setHasDeleteRequestEnded(true)
  }

  return (
    <Layout page='Recipes'>

      {deleteModal}

      <div className="flex flex-row items-end" >
        <p className="flex-grow" >Recipes</p>
        <Popup content='Add ingredient to your collection' trigger={
          <Button icon positive labelPosition='left' onClick={() => router.push(ROUTE_RESTAURANT_MENU_CREATE)}>
            <Icon name='add' />
            Add
          </Button>
        } />
      </div>

      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Title</Table.HeaderCell>
            <Table.HeaderCell>Price</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {recipes.map((element: IRecipe, index: number) =>
            <Table.Row key={element.id}
            // onClick={() => router.push(`/${ROUTE_RESTAURANT_INGREDIENT}/${element.id}/`)} 
            >
              <Table.Cell>{element.title}</Table.Cell>
              <Table.Cell>{element.price}</Table.Cell>
              <Table.Cell>
                <Icon name='eye' />
                <Icon name='edit' onClick={() => router.push(`${ROUTE_RESTAURANT_MENU_UPDATE}/${element.id}`)} />
                <Icon name='trash' onClick={() => setItemToDelete(index)} />
              </Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    </Layout>
  )
}

export default Page