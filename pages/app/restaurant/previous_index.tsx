// Import pre-installed modules
import type { NextPage } from 'next'
import Link from 'next/link'

// Import custom components
import Layout from '../../../components/Layout'

// Import config
import {
  ROUTE_RESTAURANT_MENU,
  ROUTE_RESTAURANT_INGREDIENT,
  ROUTE_RESTAURANT_ANALYTICS
} from '../../../config/routes'

// Import CSS
import { TAILWINDCSS_CLASS_BUTTON } from '../../../config/css/buttons'

const Page: NextPage = () => {
  return (
    <Layout page='Restaurant'>

      <div className='flex flex-col items-center py-3'>

        <Link href={ROUTE_RESTAURANT_INGREDIENT}>
          <div className={TAILWINDCSS_CLASS_BUTTON}>
            Ingredients
          </div>
        </Link>

        <Link href={ROUTE_RESTAURANT_MENU}>
          <div className={TAILWINDCSS_CLASS_BUTTON}>
            Recipes
          </div>
        </Link>

        <Link href={ROUTE_RESTAURANT_ANALYTICS}>
          <div className={TAILWINDCSS_CLASS_BUTTON}>
            Analytics
          </div>
        </Link>

      </div>

    </Layout>
  )
}

export default Page
