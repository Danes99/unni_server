// Import pre-installed modules
import { useState } from 'react'
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'

// Import downloaded modules
import Cookies from 'js-cookie'
import { Message } from 'semantic-ui-react'
import { GraphQLClient, gql } from 'graphql-request'
import { Player } from '@lottiefiles/react-lottie-player'

// Import custom components
import Layout from '../../../../components/Layout'
import OptionSelector from '../../../../components/OptionSelector'
import CategorySelector from '../../../../components/CategorySelector'
// import ItemSelector from '../components/ItemSelector'

// Import custom functions
import formatAPIErrorMessage from '../../../../utils/formatAPIErrorMessage'

// Import CSS
import {
  TAILWINDCSS_CLASS_TITLE_DEFAULT
} from '../../../../config/css/titles'

// Import config
import { API_ENDPOINT } from '../../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../../config/api/hostApi'

const INITIAL_STATE_TABS = [
  { name: 'Category #1', href: '#category1', current: true },
  { name: 'Category #2', href: '#category2', current: false },
  { name: 'Category #3', href: '#category3', current: false },
  { name: 'Category #4', href: '#category4', current: false }
]

const INITIAL_STATE_OPTIONS = [
  { name: 'Option #1', current: true },
  { name: 'Option #2', current: false },
  { name: 'Option #3', current: false },
  { name: 'Option #4', current: false }
]
interface IRecipe {
  id: string
  title: string
  price: number
}

interface IProps {
  recipes: [IRecipe]
}

// GraphQL query
const queryReadEveryRecipes = gql`
  query Query {
  readUserSelf {
    recipes {
      id
      title
      price
    }
  }
}`

const queryCreateOrder = gql`
mutation CreateOrder($recipeId: String!) {
  createOrder(recipeId: $recipeId) {
    id
  }
}`

// Constants - Initial State: Selected Recipes
const INITIAL_STATE_SELECTED_RECIPES: IRecipe[] = []

// Constants - Initial State: Submit recipe orders Request
const INITIAL_STATE_HAS_CREATE_REQUEST_ENDED = false
const INITIAL_STATE_HAS_CREATE_REQUEST_STARTED = false
const INITIAL_STATE_CREATE_REQUEST_ERROR = ""

// Component
const createItem = (element: IRecipe, index: number) => (
  <div key={element.id} className="border-4 rounded-lg mt-4 p-2 grid justify-items-center w-48 h-64 space-y-2" style={{ backgroundColor: '#EFF0F9' }}>
    <div className="rounded-full p-4 h-20 w-20" style={{ backgroundColor: '#00C2CB' }}>
      {/* <Image src={newItem.src} width={30} height={30}></Image> */}
    </div>
    <h2>{element.title}</h2>
    <h3>€{element.price}</h3>
  </div>
)

export const getServerSideProps: GetServerSideProps = async (context: any) => {

  const token = context.req.cookies?.jwt || null

  try {

    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', token)
    const data = await client.request(queryReadEveryRecipes)
    return { props: { recipes: data.readUserSelf.recipes } }

  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

const Page: NextPage<IProps> = ({ recipes }) => {

  // State
  const [hasCreateRequestStarted, setHasCreateRequestStarted] = useState(INITIAL_STATE_HAS_CREATE_REQUEST_STARTED)
  const [hasCreateRequestEnded, setHasCreateRequestEnded] = useState(INITIAL_STATE_HAS_CREATE_REQUEST_ENDED)
  const [createRequestError, setCreateRequestError] = useState(INITIAL_STATE_CREATE_REQUEST_ERROR)
  const [selectedRecipes, setSelectedRecipes] = useState(INITIAL_STATE_SELECTED_RECIPES.slice())
  const [, setDoRefresh] = useState(true)

  const resetStateVariables = () => {
    setHasCreateRequestStarted(INITIAL_STATE_HAS_CREATE_REQUEST_STARTED)
    setHasCreateRequestEnded(INITIAL_STATE_HAS_CREATE_REQUEST_ENDED)
    setCreateRequestError(INITIAL_STATE_CREATE_REQUEST_ERROR)
    setSelectedRecipes(current => INITIAL_STATE_SELECTED_RECIPES.slice())
    setDoRefresh(current => !current)
  }

  const onClickRecipeItemHandler = async (inputRecipe: IRecipe) => {
    selectedRecipes.push(inputRecipe)
    setDoRefresh(current => !current)
  }

  const onClickSubmitHandler = async () => {

    setHasCreateRequestStarted(true)

    try {

      const endpoint = HOST_GRAPHQL_ENDPOINT
      const jwt = Cookies.get('jwt') as string

      const promises = selectedRecipes.map(
        (element: IRecipe) => {
          const variables = { recipeId: element.id }
          const client = new GraphQLClient(
            endpoint,
            { headers: { authorization: jwt } }
          )
          return client.request(queryCreateOrder, variables)
        }
      )

      await Promise.all(promises)

    } catch (error) {
      console.error(error)
      setCreateRequestError(error instanceof Error ? error.message : 'error')
    }

    setHasCreateRequestEnded(true)
  }

  const asideContent = <>
    <span className={TAILWINDCSS_CLASS_TITLE_DEFAULT}>Order #00000</span>
    <OptionSelector options={INITIAL_STATE_OPTIONS} />
    <section className='flex flex-col self-end'>
      <ul>
        {selectedRecipes.map(
          (element: IRecipe, index: number) => <li key={index}>{element.title}</li>
        )}
      </ul>
      <p>Subtotal</p><p>0€</p>
      <p>TVA 20%</p><p>0€</p>
      <button
        type="button"
        className="rounded-l rounded-r inline-block mx-1 px-1 py-1.5 bg-indigo-600 text-white border-2 font-bold text-xs leading-tight hover:bg-indigo-800 focus:bg-indigo-600 focus:text-white focus:outline-none focus:ring-0 active:bg-indigo-600 transition duration-150 ease-in-out"
        onClick={onClickSubmitHandler}
      >
        Continue with payment
      </button>
    </section>
  </>

  return (
    <Layout page='Restaurant'>
      <div className='w-full flex flex-col tablet:flex-row tablet:gap-x-3 tablet:h-full justify-between'>
        {/* Left-side of layout - Categories and items */}
        <article className='flex flex-col tablet:block tablet:h-full tablet:w-2/3 computer:w-3/4'>
          <CategorySelector tabs={INITIAL_STATE_TABS} />
          { /* <ItemSelector /> */}
          <div className='flex gap-14 flex-wrap overflow-y-auto min-h-screen'>
            {recipes.map(
              (element: IRecipe, index: number) => <div
                key={element.id}
                onClick={() => onClickRecipeItemHandler(element)}
              >
                {createItem(element, index)}
              </div>
            )}
          </div>
          <section>

          </section>
        </article>

        {/* Right-side of layout - Ticket and Payments info */}
        <aside className='flex flex-col items-stretch rounded py-1 divide-gray-400 tablet:block tablet:h-full tablet:w-1/3 computer:w-1/4' style={{ backgroundColor: '#EFF0F9' }}>
          {hasCreateRequestStarted ?
            hasCreateRequestEnded ?
              createRequestError ?

                <div className="w-96">
                  <Player
                    src="/lottiefiles/lottiefiles_error_lf30_editor.json"
                    background="transparent"
                    speed={1} autoplay loop
                  />
                  <div className="mt-20">
                    <Message negative>
                      <Message.Header>Error</Message.Header>
                      <p>{formatAPIErrorMessage(createRequestError)}</p>
                    </Message>
                  </div>
                </div>
                :
                <Player
                  className="w-96"
                  src="/lottiefiles/lottiefiles_validated_lf30_editor.json"
                  background="transparent" speed={1} autoplay
                  onEvent={event => { if (event === 'complete') resetStateVariables() }}
                />
              :
              <Player
                className="w-96"
                src="/lottiefiles/lottiefiles_loader_several_points_lf20.json"
                background="transparent"
                speed={1} loop autoplay
              />
            :
            asideContent}
        </aside>

      </div>
    </Layout>
  )
}

export default Page
