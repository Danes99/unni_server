// Import pre-installed modules
import { useState } from 'react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { GetServerSideProps } from 'next'

// Import downloaded modules
import Cookies from 'js-cookie'
import { GraphQLClient, gql } from 'graphql-request'
import { Player } from '@lottiefiles/react-lottie-player'
import { Button, Icon, Table, Modal, Header } from 'semantic-ui-react'

// Import custom components
import Layout from '../../../../components/Layout'

// Import custom functions
import formatAPIErrorMessage from '../../../../utils/formatAPIErrorMessage'

// Import config
import { API_ENDPOINT } from '../../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../../config/api/hostApi'

interface IOrder {
  id: string
  createdAt: string
  deliveredAt: string
}

interface IRecipe {
  id: string
  title: string
  price: number
  orders: IOrder[]
}

interface IProps {
  recipes: [IRecipe]
}

const queryReadEveryOrders = gql`
query Query {
  readUserSelf {
    recipes {
      id
      title
      price
      orders {
        id
        createdAt
        deliveredAt
      }
    }
  }
}`

const queryDeleteItem = gql`
mutation Mutation($deleteOrderId: String!) {
  deleteOrder(id: $deleteOrderId) {
    id
  }
}`

const reduceRecipesToOrders = (previousValue: IOrder[], currentValue: IOrder[]) => {
  return [...previousValue, ...currentValue]
}

const convertRecipesToOrders = (recipes: IRecipe[]): IOrder[] => {

  const arrayOfArray = recipes.map((element: IRecipe): IOrder[] => element.orders)
  const _array = arrayOfArray.reduce(reduceRecipesToOrders)

  return _array
}

// Initial state: GraphQL DELETE Request (delete item)
const INITIAL_STATE_ITEM_TO_DELETE = null
const INITIAL_STATE_HAS_DELETE_REQUEST_STARTED = false
const INITIAL_STATE_HAS_DELETE_REQUEST_ENDED = false
const INITIAL_STATE_DELETE_REQUEST_ERROR = ""

export const getServerSideProps: GetServerSideProps = async (context: any) => {

  const token = context.req.cookies?.jwt || null

  try {

    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', token)
    const data = await client.request(queryReadEveryOrders)
    return { props: { recipes: data.readUserSelf.recipes } }

  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

const Page: NextPage<IProps> = ({ recipes }) => {

  // Router
  const router = useRouter()

  // State: Recipe Orders
  const [orders,] = useState(convertRecipesToOrders(recipes))

  // State: Delete request
  const [itemToDelete, setItemToDelete] = useState<number | null>(INITIAL_STATE_ITEM_TO_DELETE)
  const [hasDeleteRequestStarted, setHasDeleteRequestStarted] = useState(INITIAL_STATE_HAS_DELETE_REQUEST_STARTED)
  const [hasDeleteRequestEnded, setHasDeleteRequestEnded] = useState(INITIAL_STATE_HAS_DELETE_REQUEST_ENDED)
  const [deleteRequestError, setDeleteRequestError] = useState(INITIAL_STATE_DELETE_REQUEST_ERROR)

  const resetDeleteModal = () => {
    setItemToDelete(INITIAL_STATE_ITEM_TO_DELETE)
    setDeleteRequestError(INITIAL_STATE_DELETE_REQUEST_ERROR)
    setHasDeleteRequestEnded(INITIAL_STATE_HAS_DELETE_REQUEST_ENDED)
    setHasDeleteRequestStarted(INITIAL_STATE_HAS_DELETE_REQUEST_STARTED)
  }

  const doDisplayModal = itemToDelete !== INITIAL_STATE_ITEM_TO_DELETE

  const deleteModal = doDisplayModal ?
    <Modal
      closeIcon
      open={doDisplayModal}
      dimmer='blurring'
      onClose={resetDeleteModal}
    >
      <Header icon='trash' content={`Delete ${orders[itemToDelete].id}`} />
      <Modal.Content>

        {hasDeleteRequestStarted ?
          hasDeleteRequestEnded ?
            deleteRequestError ?
              <div className="w-96">
                <Player
                  src="/lottiefiles/lottiefiles_error_lf30_editor.json"
                  background="transparent"
                  speed={1} autoplay loop
                  onEvent={event => {
                    if (event === 'load') {
                      setTimeout(
                        () => window.location.reload(),
                        3000
                      )
                    }
                  }}
                />
                <div className="mt-20">
                  <p>{deleteRequestError}</p>
                </div>
              </div>
              :
              <Player
                className="w-96"
                src="/lottiefiles/lottiefiles_validated_lf30_editor.json"
                background="transparent"
                speed={1} autoplay
                onEvent={event => { if (event === 'complete') window.location.reload() }}
              />
            :
            <Player
              className="w-96"
              src="/lottiefiles/lottiefiles_loader_several_points_lf20.json"
              background="transparent"
              speed={1} loop autoplay
            />
          :
          <p>
            Are you sure you want to delete {orders[itemToDelete].id}?
            The ingredient will be permanently removed. This action cannot be undone.
          </p>
        }

      </Modal.Content>
      <Modal.Actions>
        <Button color='red' onClick={resetDeleteModal}>
          <Icon name='remove' /> No
        </Button>
        <Button color='green' onClick={() => onDeleteHandler(orders[itemToDelete].id)}>
          <Icon name='checkmark' /> Yes
        </Button>
      </Modal.Actions>
    </Modal>
    : null

  const onDeleteHandler = async (id: string) => {

    setHasDeleteRequestStarted(true)

    try {

      const endpoint = HOST_GRAPHQL_ENDPOINT
      const variables = { deleteOrderId: id }
      const jwt = Cookies.get('jwt') as string

      const client = new GraphQLClient(
        endpoint,
        { headers: { authorization: jwt } }
      )

      const data = await client.request(queryDeleteItem, variables)

    } catch (error) {
      console.error(error)
      setDeleteRequestError(
        formatAPIErrorMessage(
          error instanceof Error ? error.message : 'error'
        )
      )
    }

    setHasDeleteRequestEnded(true)
  }

  return (
    <Layout page='Ingredients'>

      {deleteModal}

      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Created at</Table.HeaderCell>
            <Table.HeaderCell>Delivered at</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {orders.map((element: IOrder, index: number) =>
            <Table.Row key={element.id}
            // onClick={() => router.push(`/${ROUTE_RESTAURANT_INGREDIENT}/${element.id}/`)} 
            >
              <Table.Cell>{(new Date(parseInt(element.createdAt))).toDateString()}</Table.Cell>
              <Table.Cell>{(new Date(parseInt(element.deliveredAt))).toDateString()}</Table.Cell>
              <Table.Cell>
                <Icon name='eye' />
                <Icon name='trash' onClick={() => setItemToDelete(index)} />
              </Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    </Layout>
  )
}

export default Page