// Import pre-installed modules
import Image from "next/image"
import { useState, ChangeEvent } from "react"
import type { NextPage, GetServerSideProps } from "next"

// Import downloaded modules
import Cookies from 'js-cookie'
import { GraphQLClient, gql } from 'graphql-request'

// Import downloaded modules
import Box from "@mui/material/Box"
import Badge from "@mui/material/Badge"
import { FiSettings } from "react-icons/fi"
// import Switch from "@mui/material/Switch"
// import Button from "@mui/material/Button"
// import ButtonGroup from "@mui/material/ButtonGroup"
// import FormControlLabel from "@mui/material/FormControlLabel"

import classNames from "classnames"

// Import custom components
import Layout from "../../../components/Layout"
import RequestPlayer from "../../../components/RequestPlayer"
import OptionSelector from "../../../components/OptionSelector"

// Import custom functions
import { findOccurrence } from '../../../utils/array'
import useLongPress from "../../../utils/useLongPress"

// Import CSS
// import { TAILWINDCSS_CLASS_TITLE_DEFAULT } from "../../../config/css/titles"

// Import config
import { API_ENDPOINT } from '../../../config/api'
import { HOST_GRAPHQL_ENDPOINT } from '../../../config/api/hostApi'

// const shapeStyles = { bgcolor: "primary.main", width: 40, height: 40 }
// const shapeCircleStyles = { borderRadius: "50%" }
// const rectangle = <Box component="span" sx={shapeStyles} />
// const circle = (
//   <Box component="span" sx={{ ...shapeStyles, ...shapeCircleStyles }} />
// )

// GraphQL queries
const queryReadEveryRecipes = gql`
  query Query {
  readUserSelf {
    recipes {
      id
      title
      price
    }
  }
}`

const queryCreateOrder = gql`
mutation CreateOrder($recipeId: String!) {
  createOrder(recipeId: $recipeId) {
    id
  }
}`

interface IProps {
  recipes: IRecipe[]
}

interface IRecipe {
  id: number
  title: string
  price: number
  quantity: number
  iconLink: string
  categoryId: number
}

interface IRecipeWithOccurrence extends IRecipe {
  occurrence: number
}

// list of test items
const CATEGORY_TABS = [
  {
    categoryId: 1,
    title: "Category #1",
    href: "#category1",
    current: true,
  },
  {
    categoryId: 2,
    title: "Category #2",
    href: "#category2",
    current: false,
  },
  {
    categoryId: 3,
    title: "Category #3",
    href: "#category3",
    current: false,
  },
  {
    categoryId: 4,
    title: "Category #4",
    href: "#category4",
    current: false,
  },
]

const CATEGORY_OPTIONS = [
  { name: "Option #1", current: true },
  { name: "Option #2", current: false },
  { name: "Option #3", current: false },
  { name: "Option #4", current: false },
]

// Components

interface IPropsWidgetSelectedRecipe {
  recipe: IRecipeWithOccurrence
  index: number
  count: number
  onPlusClicked: (arg0: IRecipe) => void
  onMinusClicked: (arg0: IRecipe) => void
  onCrossClicked: (arg0: IRecipe) => void
}

const SelectedRecipeDisplay = (
  { recipe, index, count, onPlusClicked, onMinusClicked, onCrossClicked }: IPropsWidgetSelectedRecipe
) => {

  const [notes, setData] = useState("")

  // const addQuantity = () => {
  //   if (quantity < 99) {
  //     setQuantity(quantity + 1)
  //   }
  // }

  const removeItemFromCart = () => { }

  // const remQuantity = () => {
  //   if (quantity != 1) {
  //     setQuantity(quantity - 1)
  //   }
  // }

  const handleNoteChange = (e: ChangeEvent<HTMLInputElement>) => {
    setData(e.target.value)
  }

  const plusButtonOnClickHandler = (event: any) => {
    onPlusClicked(recipe as IRecipe)
  }

  const minusButtonOnClickHandler = (event: any) => {
    onMinusClicked(recipe as IRecipe)
  }

  return (
    <div key={index} className="flex flex-col w-full gap-1">
      <div className="flex flex-row gap-2 items-center justify-between mr-1">
        {/* Product information */}
        <section className="flex flex-row gap-2 w-1/2 flex-wrap">
          <div className="flex justify-center content-center rounded-full h-10 w-10 bg-blue-unni">
            {recipe.iconLink && <Image src={recipe.iconLink} width={30} height={30}></Image>}
          </div>
          <div className="inline-flex flex-col grow">
            <span>{recipe.title}</span>
            <span className="text-xs">{recipe.price.toFixed(2)}€</span>
          </div>
        </section>

        {/* Quantity */}
        <section className="flex flex-row gap-1 w-1/4 flex-wrap">
          <div className="flex flex-row h-10 w-10 rounded-lg relative bg-transparent mt-1">
            <button
              onClick={minusButtonOnClickHandler}
              data-action="decrement"
              className=" bg-gray-200 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-content rounded-l cursor-pointer outline-none"
            >
              <span className="m-auto text-2xl font-thin">−</span>
            </button>
            <input
              onChange={() => { }}
              type="text"
              className="form-control text-center font-semibold text-md w-12 text-gray-700 bg-gray-200 bg-clip-padding border border-solid border-gray-300 transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              value={count}
            ></input>
            <button
              onClick={plusButtonOnClickHandler}
              data-action="increment"
              className="bg-gray-200 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-content rounded-r cursor-pointer"
            >
              <span className="m-auto text-2xl font-thin">+</span>
            </button>
          </div>
        </section>

        {/* Price */}

        <section className="text-center mx-1 w-1/4">
          <span className="font-bold">
            {(recipe.price * count).toFixed(2)}€
          </span>
        </section>
      </div>
      <div className="w-full flex flex-row gap-2">
        <input
          onChange={handleNoteChange}
          value={notes}
          type="text"
          className="form-control w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-gray-200 bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
          placeholder="Note de commande"
        />
        <button
          onClick={(event) => onCrossClicked(recipe)}
          className="rounded-l rounded-r w-10 h-10 bg-gray-200 border-2 border-red-500 font-bold text-xs leading-tight hover:bg-red-500 transition duration-150 ease-in-out"
        >
          <div className="h-auto w-auto flex justify-center">
            <Image src="/remove.png" width={20} height={20} />
          </div>
        </button>
      </div>
    </div>
  )
}

interface ISelectedRecipeForm {
  recipes: IRecipe[]
  onAdd: (arg0: IRecipe) => void
  onRemove: (arg0: IRecipe) => void
  onDrop: (arg0: IRecipe) => void
  onSubmit: () => void
}

const SelectedRecipeForm = ({ recipes, onAdd, onRemove, onDrop, onSubmit }: ISelectedRecipeForm) => {

  // const occurrenceArray = findOccurrence(recipes, "title")
  const occurrenceArray = findOccurrence(recipes, "id")

  const displayedSelectedRecipes = occurrenceArray.map(
    (element: any, index: number) => (
      <SelectedRecipeDisplay
        key={index}
        recipe={element as IRecipeWithOccurrence}
        count={element.occurrence}
        index={index}
        onPlusClicked={(recipe: IRecipe) => onAdd(recipe)}
        onMinusClicked={(recipe: IRecipe) => onRemove(recipe)}
        onCrossClicked={(recipe: IRecipe) => onDrop(recipe)}
      />
    )
  )

  const Subtotal = recipes.reduce((sum, element) => (sum += element.price), 0).toFixed(2)
  const VAT = (recipes.reduce((sum, element) => (sum += element.price), 0) * 0.2).toFixed(2)

  return <>
    <section className="mt-5 justify-self-center">
      <span className="w-full text-2xl text-center text-gray-800">
        Order #00000
      </span>
    </section>
    <section className="mt-2">
      <OptionSelector options={CATEGORY_OPTIONS} />
    </section>

    <section className="mt-2">
      <div className="flex flex-row justify-items-between">
        <span className="w-3/5 text-2xl text-left text-gray-800 grow">
          Products
        </span>
        <span className="w-1/5 text-2xl text-left text-gray-800 shrink">
          QT
        </span>
        <span className="w-1/5 text-2xl text-left text-gray-800">
          Price
        </span>
      </div>
      <hr className="bg-gray-500 h-0.5 rounded-lg my-3" />

      <section className="flex flex-col w-full gap-5">
        {displayedSelectedRecipes}
      </section>
    </section>

    <section className="self-end w-full inline-flex flex-col">
      <hr className="bg-gray-500 h-0.5 rounded-lg mb-3" />
      <div className="flex flew-row max-h-max justify-between mx-2">
        <span className="text-gray-600">Subtotal</span>
        <span className="text-align-right justify-self-end text-blue-unni">
          {Subtotal}€
        </span>
      </div>
      <div className="flex flew-row max-h-max justify-between mx-2 py-2">
        <span className="text-gray-600">TVA 20%</span>
        <span className="text-align-right text-blue-unni">
          {VAT}€
        </span>
      </div>
      <button
        type="button"
        onClick={onSubmit}
        className="font-montserrat rounded-md px-1 py-1.5 shadow-xl h-10 
        bg-blue-unni text-white border-2 font-bold text-xs leading-tight 
        hover:bg-indigo-800 focus:bg-blue-unni focus:text-white focus:outline-none focus:ring-0 £
        active:bg-blue-unni transition duration-150 ease-in-out"
      >
        Continue with payment
      </button>
    </section>
  </>
}

interface IWidgetRecipeProps {
  recipe: IRecipe
}

const WidgetRecipe = ({ recipe }: IWidgetRecipeProps) => {

  const [count, setCount] = useState(0)
  const [invisible, setInvisible] = useState(false)

  const handleBadgeVisibility = () => {
    setInvisible(!invisible)
  }

  const onLongPress = () => {
    setCount(Math.max(count - 1, 0))
    if (count <= 0) {
      setInvisible(invisible)
    }
  }

  const onClick = () => {
    // console.log('click is triggered')
    setCount(count + 1)
    // setInvisible(!invisible)
  }

  const defaultOptions = {
    shouldPreventDefault: true,
    delay: 500,
  }

  const longPressEvent = useLongPress(onLongPress, onClick, defaultOptions)

  return (
    <Badge
      color="error"
      badgeContent={count}
      max={99}
      className="mt-4"
      invisible={invisible}
    >
      <div key={recipe.id} className='border-2 border-blue-unni 
        rounded-lg p-2 flex flex-col justify-between items-center w-36 h-36 bg-gray-unni
        computer:w-44 computer:h-44 computer:border-4 computer:space-y-2
        tablet:w-44 tablet-44 tablet:border-4 tablet:space-y-2'
      >
        <div
          className='phone:text-xs
          tablet:text-base
          computer:text-base'
        >
          {recipe.title}
        </div>
        <div className='rounded-full h-12 w-12 bg-white 
          border-blue-unni border-2 flex flex-col items-center justify-center
          tablet:border-4 tablet:h-20 tablet:w-20
          computer:border-4 computer:h-20 computer:w-20'
        >
          {recipe.iconLink &&
            <Image src={recipe.iconLink} width={25} height={25}></Image>}
        </div>
        <div className='space-x-2 text-xs'>
          <span>{recipe.price.toFixed(2)}€</span>
          {/* <span>-</span> */}
          {/* <span>{quantity} produit(s)</span> */}
        </div>
      </div>
    </Badge>
  )
}

interface ICategory {
  categoryId: number
  title: string
  href: string
  current: boolean
}

interface IPropsCategory {
  tabs: ICategory[]
}

const SelectorCategory = ({ tabs }: IPropsCategory) => {

  const [, setDoRefresh] = useState(true)

  const updateTabHandler = (index: number) => {
    tabs.forEach((element: ICategory) => { element.current = false })
    tabs[index].current = true
    setDoRefresh((current) => !current)
  }

  return (
    <section className="border-b border-gray-200">
      <nav
        className="font-montserrat flex space-x-8 justify-evenly"
        aria-label="Tabs"
      >
        {tabs.map((tab: ICategory, index: number) => (
          <a
            key={tab.title}
            href={tab.href}
            className={classNames(
              tab.current
                ? "border-blue-unni border-b-4 text-blue-unni"
                : "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300",
              "whitespace-nowrap py-4 px-1 border-b-4 font-medium text-sm flex-auto text-center"
            )}
            aria-current={tab.current ? "page" : undefined}
            onClick={(event) => updateTabHandler(index)}
          >
            {tab.title}
          </a>
        ))}
        {/* Side Bar Icon */}
        <div className="sidebar-icon">
          <FiSettings size="30" />
        </div>
      </nav>
    </section>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: any) => {
  try {
    const client = new GraphQLClient(API_ENDPOINT)
    client.setHeader('authorization', context.req.cookies?.jwt)
    const data = await client.request(queryReadEveryRecipes)
    return { props: { recipes: data.readUserSelf.recipes } }
  } catch (error) {
    console.error(error)
    return { props: {} }
  }
}

// Initial State
const INITIAL_STATE_ERROR_MESSAGE = ''
const INITIAL_STATE_REQUEST_STATUS = 'NOT_STARTED'
const INITIAL_STATE_SELECTED_RECIPES: IRecipe[] = []

const Restaurant: NextPage<IProps> = ({ recipes }) => {

  // State
  const [requestStatus, setRequestStatus] = useState<string>(INITIAL_STATE_REQUEST_STATUS)
  const [errorMessage, setErrorMessage] = useState<string>(INITIAL_STATE_ERROR_MESSAGE)
  const [selectedRecipes, setSelectedRecipes] = useState<IRecipe[]>(INITIAL_STATE_SELECTED_RECIPES.slice())
  const [, setDoRefresh] = useState<boolean>(true)

  const resetDisplay = () => {
    setErrorMessage(INITIAL_STATE_ERROR_MESSAGE)
    setRequestStatus(INITIAL_STATE_REQUEST_STATUS)
    setSelectedRecipes(INITIAL_STATE_SELECTED_RECIPES.slice())
  }

  const onClickAddRecipeHandler = async (recipe: IRecipe) => {
    selectedRecipes.push(recipe)
    setDoRefresh(current => !current)
  }

  const onClickRemoveRecipeHandler = async (recipe: IRecipe) => {
    const index = selectedRecipes.findIndex(
      (value: IRecipe, index: number, array: IRecipe[]) => value.id === recipe.id
    )
    selectedRecipes.splice(index, 1)
    setDoRefresh(current => !current)
  }

  const onClickRemoveEveryRecipeHandler = async (recipe: IRecipe) => {
    setSelectedRecipes(
      current => selectedRecipes.filter(
        (value: IRecipe, index: number, array: IRecipe[]) => value.id !== recipe.id
      )
    )
  }

  console.log()

  const onSubmitHandler = async () => {
    try {
      setErrorMessage('')
      setRequestStatus('LOADING')

      await Promise.all(selectedRecipes.map(
        (element: IRecipe) => {
          const variables = { recipeId: element.id }
          const client = new GraphQLClient(
            HOST_GRAPHQL_ENDPOINT,
            { headers: { authorization: Cookies.get('jwt') as string } }
          )
          return client.request(queryCreateOrder, variables)
        }
      ))

      setRequestStatus('DONE')

    } catch (error: any) {
      console.error(error)
      setRequestStatus('ERROR')
      setErrorMessage(error.message || 'error')
    }
  }

  return (
    <Layout page="Restaurant">
      <div className="font-montserrat w-full flex flex-col 
        tablet:flex-row tablet:gap-x-3 tablet:h-full 
        justify-between overflow-hidden"
      >
        {/* Left-side of layout - Categories and items */}
        <article
          className="flex flex-col 
          tablet:h-full tablet:w-2/3 tablet:overflow-auto 
          computer:w-3/4 computer:overflow-auto"
        >
          <div className="flex justify-between tablet:hidden computer:hidden gap-8">
            <button className="border rounded-lg w-full flex justify-between p-2">
              Rechecher
              <Image src={"/search.png"} width={20} height={20}></Image>
            </button>
            <div className="flex self-center">
              <Image src={"/cart.png"} width={30} height={30}></Image>
            </div>
          </div>
          <SelectorCategory tabs={CATEGORY_TABS} />
          <section className="h-full">
            {CATEGORY_TABS.map((tab: ICategory, index: number) => (
              <div
                key={index}
                id={tab.href}
                className={classNames(
                  tab.current ? "show active" : "hidden",
                  "flex flex-wrap gap-2 justify-start p-2"
                )}
              >
                {recipes.map((element: IRecipe, index: number) =>
                  <div onClick={() => onClickAddRecipeHandler(element)} key={index}>
                    <WidgetRecipe recipe={element} />
                  </div>
                )}
              </div>
            ))}
          </section>
        </article>

        {/* Right-side of layout - Ticket and Payments info */}
        <aside
          className="hidden flex-col gap-5 rounded-xl px-5 py-1 
            tablet:block computer:block tablet:h-full tablet:w-1/3 
            computer:w-1/4 tablet:overflow-auto computer:overflow-auto"
          style={{ backgroundColor: "#EFF0F9" }}
        >
          <RequestPlayer
            status={requestStatus}
            callbackSuccess={resetDisplay}
            callbackError={resetDisplay}
          >
            <SelectedRecipeForm
              recipes={selectedRecipes}
              onAdd={onClickAddRecipeHandler}
              onRemove={onClickRemoveRecipeHandler}
              onDrop={onClickRemoveEveryRecipeHandler}
              onSubmit={onSubmitHandler}
            />
          </RequestPlayer>
        </aside>

      </div>
    </Layout>
  )
}

export default Restaurant
