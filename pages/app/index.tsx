// Import pre-installed modules
import Link from 'next/link'
import type { NextPage } from 'next'

// Import custom components
import Layout from '../../components/Layout'

// Import config
import { ROUTE_RESTAURANT } from '../../config/routes'

// Import CSS
import { TAILWINDCSS_CLASS_BUTTON } from '../../config/css/buttons'

const Page: NextPage = () => {
  return (
    <Layout page='App'>

      <div className='flex flex-col items-center py-3'>

        <Link href={ROUTE_RESTAURANT}>
          <div className={TAILWINDCSS_CLASS_BUTTON}>
            Restaurant
          </div>
        </Link>

        <Link href={ROUTE_RESTAURANT}>
          <div className={TAILWINDCSS_CLASS_BUTTON}>
            Hotel
          </div>
        </Link>

        <Link href={ROUTE_RESTAURANT}>
          <div className={TAILWINDCSS_CLASS_BUTTON}>
            Location
          </div>
        </Link>

      </div>

    </Layout>
  )
}

export default Page
