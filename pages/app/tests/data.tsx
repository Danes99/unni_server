// Import pre-installed modules
import type { NextPage } from 'next'

// Import custom components
import Layout from '../../../components/Layout'
import DataCollectionWrapper from '../../../components/DataCollectionWrapper'

// Import config
// import { ROUTE_RESTAURANT } from '../../../config/routes'

// Import CSS
import { TAILWINDCSS_CLASS_BUTTON } from '../../../config/css/buttons'

const Page: NextPage = () => {
  return (

    <DataCollectionWrapper>
      <Layout page='App'>

        <div className='flex flex-col items-center py-3'>

          {/* <Link href={ROUTE_RESTAURANT}> */}
          <div
            id='Hover'
            className={TAILWINDCSS_CLASS_BUTTON}>
            Hover
          </div>
          {/* </Link> */}

          {/* <Link href={ROUTE_RESTAURANT}> */}
          <div
            id='Click'
            className={TAILWINDCSS_CLASS_BUTTON}
          >
            Click
          </div>
          {/* </Link> */}

          {/* <Link href={ROUTE_RESTAURANT}> */}
          <div
            className={TAILWINDCSS_CLASS_BUTTON}
            onClick={() => { throw new Error('Test Error') }}
          >
            Throw Error
          </div>
          {/* </Link> */}

        </div>

      </Layout>
    </DataCollectionWrapper>
  )
}

export default Page
