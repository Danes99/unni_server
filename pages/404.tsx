// Import pre-installed modules
import { useRouter } from 'next/router'

// Import downloaded modules
import { Player } from '@lottiefiles/react-lottie-player'

// Import custom components
import Layout from '../components/Layout'

// Import config
import { ROUTE_HOME } from '../config/routes'

export default function Custom404() {

  // Next.js Router
  const router = useRouter()

  return <Layout page='404'>
    <div
      onClick={() => router.push(ROUTE_HOME)}
    >
      <Player
        className="w-4/5"
        src="/lottiefiles/lottiefiles_lf30_404.json"
        background="transparent"
        speed={1} autoplay loop

      // onEvent={event => { if (event === 'complete') router.push(ROUTE_HOME) }}
      />
    </div>
  </Layout>
}