// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

// Import downloaded modules
import { GraphQLClient, gql } from 'graphql-request'

// Import config
import { API_ENDPOINT } from '../../config/api/index'

type IHttpResponse = {
  data: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IHttpResponse>
) {

  if (!req.body.query) {
    return res.status(200).json({ data: 'No query' })
  }

  const client = new GraphQLClient(API_ENDPOINT)

  client.setHeader(
    'authorization',
    req.headers.authorization || ''
  )

  const data = await client.request(
    req.body.query,
    req.body.variables
  )

  return res.json({ data })
}
