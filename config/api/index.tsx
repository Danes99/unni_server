// GraphQL API
// export const API_PROTOCOL = 'http:'
export const API_PORT = process.env.API_PORT || 8082
export const API_PROTOCOL = process.env.API_PROTOCOL || 'http'
export const API_BASE_URL = process.env.API_BASE_URL || 'localhost'

// API url
export const API_URL = `${API_PROTOCOL}://${API_BASE_URL}:${API_PORT}/`

// API endpoints
export const GRAPHQL_ENDPOINT = process.env.GRAPHQL_ENDPOINT || 'graphql/'
export const API_ENDPOINT = API_URL + GRAPHQL_ENDPOINT