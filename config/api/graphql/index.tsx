

// Import downloaded modules
import { gql } from 'graphql-request'

export const readUserSelf = gql`
  query ReadUserSelf {
    readUserSelf {
      id
      createdAt
      updatedAt
      name
      email
      role
    }
  }
`

export const userLogin = gql`
  mutation UserLogin($email: String!, $password: String!) {
    userLogin(email: $email, password: $password) {
      token,
      expiration
    }
  }
`

export const createUser = gql`
  mutation CreateUser($name: String!, $email: String!, $password: String!) {
    createUser(name: $name, email: $email, password: $password) {
      expiration
      token
    }
  }
`