
export const TAILWINDCSS_CLASS_BUTTON = `
  transition duration-500 inline-flex justify-center 
  my-4 py-3 w-36 border border-transparent rounded-md shadow-sm 
  text-sm text-white font-medium 
  bg-blue-400 hover:bg-blue-600 
  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500`