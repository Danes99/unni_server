// Import config
import { ROUTE_USER_LOGIN } from './routes'

export const REDIRECT = {
  redirect: {
    destination: ROUTE_USER_LOGIN,
    permanent: false,
  },
}