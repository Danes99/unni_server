// Basics
export const ROUTE_HOME = '/'
export const ROUTE_ABOUT = '/about'
export const ROUTE_HELP = '/help'

// Authentication
export const ROUTE_AUTHENTICATION = '/auth'
export const ROUTE_USER_LOGIN = ROUTE_AUTHENTICATION + '/login'
export const ROUTE_USER_REGISTER = ROUTE_AUTHENTICATION + '/register'

export const ROUTE_APP = '/app'

// User
export const ROUTE_USER = ROUTE_APP + '/user'

// Restaurant
export const ROUTE_RESTAURANT = ROUTE_APP + '/restaurant'
// Restaurant: Ingredient
export const ROUTE_RESTAURANT_INGREDIENT = ROUTE_RESTAURANT + '/ingredient'
export const ROUTE_RESTAURANT_INGREDIENT_CREATE = ROUTE_RESTAURANT_INGREDIENT + '/create'
export const ROUTE_RESTAURANT_INGREDIENT_UPDATE = ROUTE_RESTAURANT_INGREDIENT + '/update'
// Restaurant: Menu
export const ROUTE_RESTAURANT_MENU = ROUTE_RESTAURANT + '/menu'
export const ROUTE_RESTAURANT_MENU_CREATE = ROUTE_RESTAURANT_MENU + '/create'
export const ROUTE_RESTAURANT_MENU_UPDATE = ROUTE_RESTAURANT_MENU + '/update'
// Restaurant: Analytics
export const ROUTE_RESTAURANT_ANALYTICS = ROUTE_RESTAURANT + '/analytics'

// Test Routes
export const ROUTE_TEST = '/test'
export const ROUTE_TEST_RESTAURANT = ROUTE_TEST + '/restaurant'
export const ROUTE_TEST_HOTEL = ROUTE_TEST + '/hotel'
export const ROUTE_TEST_STOCK = ROUTE_TEST + '/stock'
export const ROUTE_TEST_MENU = ROUTE_TEST + '/menu'
export const ROUTE_TEST_DASHBOARD = ROUTE_TEST + '/dashboard'